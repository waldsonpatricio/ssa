#include "gtest/gtest.h"
#include "Crustal.h"

TEST(Crustal, mustReturnSameInstance)
{
    auto& c  = Crustal::getInstance();
    auto& c2 = Crustal::getInstance();
    auto& c3 = Crustal::getInstance();

    ASSERT_EQ(&c, &c2);
    ASSERT_EQ(&c2, &c3);
}

TEST(Station, mustCalculateTravelTimeWhenComputingBetweenDifferentLayers)
{
    // dados da estação, camadas e ponto coletados
    // a partir do caso que estava dando errado
    Station s;
    s.name = "Test Station";
    s.x    = 0.792f;
    s.y    = 0.396f;
    s.z    = 0.051f;


    auto& c = Crustal::getInstance();
    c.addLayer(0.0f, 1.615f, 0.950f);
    c.addLayer(0.3f, 2.398f, 1.411f);
    c.addLayer(0.9f, 3.134f, 1.843f);


    ASSERT_NO_THROW({
        auto result = s.computeTravelTimeToPoint(c, -3, -3, 0.05f);

        auto pWave = result.pWave;
        auto sWave = result.sWave;

        //Valor calculado manualmente e validado
        ASSERT_NEAR(2.213f, pWave, 1.0f);
        ASSERT_NEAR(3.759f, sWave, 1.0f);
    });

    ASSERT_NO_THROW({
        auto result = s.computeTravelTimeToPoint(c, -3, -2.7f, 0.05f);

        auto pWave = result.pWave;
        auto sWave = result.sWave;

        //Valor calculado manualmente e validado
        ASSERT_NEAR(2.215f, pWave, 1.0f);
        ASSERT_NEAR(3.653f, sWave, 1.0f);
    });

    ASSERT_NO_THROW({
        auto result = s.computeTravelTimeToPoint(c, -3, -2.4f, 0.05f);

        auto pWave = result.pWave;
        auto sWave = result.sWave;

        //Valor calculado manualmente e validado
        ASSERT_NEAR(2.092f, pWave, 1.0f);
        ASSERT_NEAR(3.530f, sWave, 1.0f);
    });


    ASSERT_NO_THROW({
        auto result = s.computeTravelTimeToPoint(c, -3, -2.7f, 0.350f);

        auto pWave = result.pWave;
        auto sWave = result.sWave;

        //Valor calculado manualmente e validado
        ASSERT_NEAR(2.004f, pWave, 0.2f);
        ASSERT_NEAR(3.405f, sWave, 0.2f);
    });

    ASSERT_NO_THROW({
        auto result = s.computeTravelTimeToPoint(c, -3, -2.7f, 0.650f);

        auto pWave = result.pWave;
        auto sWave = result.sWave;

        //Valor calculado manualmente e validado
        ASSERT_NEAR(1.986f, pWave, 0.2f);
        ASSERT_NEAR(3.374f, sWave, 0.2f);
    });
}