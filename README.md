# Build #

``mkdir build``

`cd build`

`cmake ..`

`make`

This will generate a ssa executable inside build folder.

# Run #

You have to create a config file before run:

`./ssa init`

This will generate a `ssa.json` file. Edit this file to set execution
parameters. This files can contain the following properties:

|                  Property | Description                                                                                                                                                                                      |
|                ---------- | -------------                                                                                                                                                                                    |
|              `averageEvery` | `integer` Number of OTs that should be used to take an average.  0 means all OTS                                                                                                               |
|      `averageMinBrightness` | `float` Minimum brightness necessary for an OT processing to be taken into account when generating average.                                                                                    |
| `brightnessCutoffMultipler` | `float` Defines which points should be used to generate statistics about the OT result. E.g.: `0.8` means that only points with brightness with at least `80%` of maximum brightness value will be taken into account. |
|                   `crustal` | `array` An array of layers defining the velocity model. Each item is an `object` containing depth (`depth`), primary wave speed (`pw`), and secondary wave speed (`sw`).                             |
|            `fileSaveFilter` | `object` Defines which files must be saved after run. Each property receives a `float` indicating the minimum value that the result must have for this particular property to generate a file. |
|                   `firstOt` | `integer` Defines the starting OT.                                                                                                                                                             |
|                  `inputDir` | `string` Path where stations files and `station.dat` file are located.                                                                                                                         |
|        `maxFileSaveThreads` | `integer` Number of threads used to save files.                                                                                                                                                |
|                    `maxOts` | `integer` Number of OTs that must be processed.                                                                                                                                                |
|                 `outputDir` | `string` Path where result files must be saved                                                                                                                                                 |
|               `parallelOts` | `integer` Number of OTs that will be stacked in parallel at each run.                                                                                                                          |
|                     `range` | `object` Defines search area.                                                                                                                                                                  |
|              `samplingRate` | `float` Defines the sampling interval of stations.                                                                                                                                             |
|                `sequential` | `boolean` Optional. If `true` runs using sequential version.                                                                                                                                   |
|       `stationFileTemplate` | `string`  Template that maps station name in `stations.dat` to a station log file. This will replace `{name}` placeholder with the name of the stations.                                         |


# Caveats

- This will work only with CUDA 8 (CUDA 9 and 10 has a bug where they can't handle large indexes):
    - Bug: https://github.com/thrust/thrust/issues/967
    - Extra info:
        - https://ubuntuforums.org/showthread.php?t=2331026
        - https://devtalk.nvidia.com/default/topic/983777/cuda-setup-and-installation/can-t-locate-installutils-pm-in-inc/
- CUDA 8 only works with gcc 5.3


# Run with Docker

- https://medium.com/@sh.tsang/docker-tutorial-5-nvidia-docker-2-0-installation-in-ubuntu-18-04-cb80f17cac65
