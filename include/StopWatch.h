#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <chrono>

class StopWatch {
    public:
        StopWatch();
        StopWatch(const StopWatch& orig) = delete;
        void start();
        void stop();
        
        double getTimeElasped();
        
    private:
        bool running_;
        std::chrono::high_resolution_clock::time_point start_;
        std::chrono::high_resolution_clock::time_point end_;
};

#endif 

