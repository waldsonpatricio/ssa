#include <thrust/host_vector.h>

void compute_ots(
    size_t numberOfStations, 
    size_t numberOfLogsPerStation,
    size_t numberOfSamplesPerOt, 
    size_t numberOfOts, 
    size_t firstOt, //offset
    const thrust::host_vector<float>& values, 
    const thrust::host_vector<size_t>& distanceOffsets,
    thrust::host_vector<float>& result
);

