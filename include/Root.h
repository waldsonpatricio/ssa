#ifndef ROOT_H
#define ROOT_H

#include <complex>

struct Root 
{
    enum class Type 
    {
        SingleRealPole,
        ComplexConjugatePolePair,
        ComplexConjugatePoleZeroPair
    };
    
    std::complex<float> complex;
    Type type;
};


#endif /* ROOT_H */

