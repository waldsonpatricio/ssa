#ifndef CLASS_TEMPLATES_H
#define CLASS_TEMPLATES_H

#include <fstream>
#include <string>
#include <stdexcept>

template<typename F = std::fstream>
class Opener {
    public:
        Opener(std::string filename) : stream(filename)
        {
            if (!stream.is_open()) {
                throw std::invalid_argument{
                    std::string{"File '" + filename + "' could not be opened or was not found."}
                };
            }
        }
        
        Opener(const Opener<F>& other)            = delete;
        Opener& operator=(const Opener<F>& right) = delete;

        
        F& operator*() 
        {
            return stream;
        }
        
        F* operator->() 
        {
            return &stream;
        }
        
        ~Opener() {
            if (stream.is_open()) {
                stream.close();
            }
        }
        
    private:
        F stream;
};


#endif /* CLASS_TEMPLATES_H */

