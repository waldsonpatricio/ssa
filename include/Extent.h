#ifndef EXTENT_H
#define EXTENT_H

#include <algorithm>
#include <iostream>

struct Extent {

    float min;
    float max;
    float step;


    Extent() :
        min(0),
        max(0),
        step(0),
        numberOfSamples_(0)
    {}


    Extent(float minimun, float maximum, float stp) :
        min{minimun},
        max{maximum},
        step(stp)
    {
        numberOfSamples_ = calculateNumberOfSamples();
        std::cout << "Number: " << numberOfSamples_ << std::endl;
    }

    size_t getNumberOfSamples() const
    {
        if (numberOfSamples_ == 0) {
            numberOfSamples_ = calculateNumberOfSamples();
        }
        return numberOfSamples_;
    }

    private:
        mutable size_t numberOfSamples_;
        size_t calculateNumberOfSamples() const
        {
            if (min >= max || step == 0 || step > (max - min)) {
                return 0;
            }
            /* return static_cast<size_t>((max - min) / step + 1); */
            /* return static_cast<size_t>((max - min) / step); */

            //fazendo com for porque tem mais precisão
            size_t samples = 0;
            for (float i = min; i <= max - 0.0000001; i += step) {
                ++samples;
            }
            return samples + 1;
        }
};


#endif /* EXTENT_H */
