#ifndef TIMEFILTER_H
#define TIMEFILTER_H

#include <memory>
#include <vector>
#include <cmath>
#include <utility>
#include <stdexcept>
#include <string>
#include <iostream>
#include <complex>

#include "math_constants.h"
#include "Station.h"
#include "Coefficient.h"
#include "Root.h"
#include "functions.h"

class TimeFilter {
    public:
        
        enum class Algorithm {
            Butterworth,
            Bessel,
            ChebyshevTypeI,
            ChebyshevTypeII
        };
        
        enum class Type {
            LowPass,
            HighPass,
            BandPass,
            BandReject
        };
        
        enum class PassType {
            Forward,
            ForwardAndReverse
        };
        
        TimeFilter(
            Algorithm algo,
            Type type,
            PassType passType,
            float transitionBandwidth,
            float attenuationFactor,
            unsigned int order,
            float lowFrequencyCutOff,
            float highFrequencyCutOff,
            float samplingInterval
        );
        float getSamplingInterval() const 
        {
            return samplingInterval_;
        }
        /**
         * Computing 'power' of the original data (the used stations only !)
         * @param logs
         * @return float the 'power'of the original data
         */
        float operator()(std::vector<Station::Log>& logs) const;
        
    private:
        inline Root createRoot(float real, float imaginary, Root::Type type);
        inline Coefficient createCoefficient(float numerator, float denominator);
        
        std::vector<Root> setupButterWorth(float& dcValue);
        std::vector<Root> setupBessel(float& dcValue);
        std::vector<Root> setupChebyshevTypeI(float& dcValue);
        std::vector<Root> setupChebyshevTypeII(float& dcValue, std::vector<std::complex<float>>& zeros);
        
        std::vector<Coefficient> lowPassToBandPass(
            const std::vector<Root>& roots, 
            const std::vector<std::complex<float>> zeros, 
            float dcValue,
            float lowFrequencyCutoff,
            float highFrequencyCutoff
        );
        
        std::vector<Coefficient> lowPassToHighPass(
            const std::vector<Root>& roots, 
            const std::vector<std::complex<float>> zeros, 
            float dcValue
        );
        
        std::vector<Coefficient> lowPass(
            const std::vector<Root>& roots, 
            const std::vector<std::complex<float>> zeros, 
            float dcValue
        );
        
        void cutoffs(std::vector<Coefficient>& coefficients, float cutoffFrequency);
        void bilinear(std::vector<Coefficient>& coefficients);
        
        std::vector<Coefficient> lowPassToBandReject(
            const std::vector<Root>& roots, 
            const std::vector<std::complex<float>> zeros, 
            float dcValue,
            float lowFrequencyCutoff,
            float highFrequencyCutoff
        );
        
        
        inline int trunc(float value);
        inline void addRoot(std::vector<Root>& root, float real, float imaginary, Root::Type type);
        inline void addCoefficient(std::vector<Coefficient>& coefficients, float numerator, float denominator);
        
        Type type_;
        Algorithm algorithm_;
        PassType passType_;
        float transitionBandwidth_;
        float attenuationFactor_;
        unsigned int order_;
        float lowFrequencyCutOff_;
        float highFrequencyCutOff_;
        float samplingInterval_;
        std::vector<Coefficient> coefficients_;
        
};

#endif /* TIMEFILTER_H */

