#ifndef SSARESULT_H
#define SSARESULT_H

#include <limits>
#include <memory>
#include <utility>
#include <vector>
#include <thrust/host_vector.h>


#include "json.hpp"
#include "functions.h"

class SSA;

class SSAResult {
    friend class SSA;
    friend void processResult(const thrust::host_vector<float>& stack, const SSA& ssa, const size_t firstOt, const nlohmann::json& config, std::string output_dir);

    public:
        struct Info {
            float x;
            float y;
            float z;
            size_t stationsUsedP;
            size_t stationsUsedS;
            float  sumP;
            float  sumS;
            float goodP;
            float goodS;
            float goodPs;

            Info(float x_, float y_, float z_, size_t stationsUsedP_, size_t stationsUsedS_,
                 float sumP_, float sumS_, float goodP_, float goodS_, float goodPs_
            );

            Info();
        };

        SSAResult();

        void addInfo(float x, float y, float z, size_t stationsUsedP, size_t stationsUsedS,
                 float sumP, float sumS, float goodP, float goodS, float goodPs);

        const Info& getInfoAt(size_t index) const;

        const Info& getMaximumPWaveBrightnessInfo() const;
        const Info& getMaximumSWaveBrightnessInfo() const;
        const Info& getMaximumPsWaveBrightnessInfo() const;

        const size_t getMaximumPWaveBrightnessInfoIndex() const;
        const size_t getMaximumSWaveBrightnessInfoIndex() const;
        const size_t getMaximumPsWaveBrightnessInfoIndex() const;

        const size_t getNumP() const;
        const size_t getNumS() const;
        const size_t getNumPS() const;

        const std::vector<std::unique_ptr<Info>>& getInfos() const;

        float getRatNumP() const;
        float getRatGoodP() const;
        float getRatNumS() const;
        float getRatGoodS() const;
        float getRatNumPs() const;
        float getRatGoodPs() const;

        /**
         * @return float execution time in seconds
         */
        float getExecutionTime() const;


    private:
        std::vector<std::unique_ptr<Info>> infos_;
        size_t maximumPWaveBrightnessInfoIndex_;
        size_t maximumSWaveBrightnessInfoIndex_;
        size_t maximumPsWaveBrightnessInfoIndex_;
        float ratNumP_;
        float ratGoodP_;
        float ratNumS_;
        float ratGoodS_;
        float ratNumPs_;
        float ratGoodPs_;
        float executionTime_;
        size_t numP_;
        size_t numS_;
        size_t numPS_;
};

#endif /* SSARESULT_H */
