#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <memory>
#include <cmath>
#include "math_constants.h"

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

/**
 * Applies tangent frequency warping to compensate
 * for bilinear analog -> digital transformation
 * @param frequency Original design frequency specification in hertz
 * @param samplingInteval Sampling interval in seconds
 * @return 
 */
float warp(float frequency, float samplingInteval);

/**
 * Calculates Chebyshev type I and II passband parameter
 * @param stopBandAttenuation 
 * @param transitionBandwidth
 * @param order
 * @return 
 */
float chebyshevPassband(float stopBandAttenuation, float transitionBandwidth, unsigned int order);

/**
 * Calculates Chebyshev type I and II ripple parameter
 * @param passband
 * @return 
 */
float chebyshevRipple(float passband);

#endif

