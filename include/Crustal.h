#ifndef CRUSTAL_H
#define CRUSTAL_H

#include <algorithm>
#include <memory>
#include <vector>
#include <stdexcept>
#include <string>
#include <functional>

#include "functions.h"
#include "Station.h"
#include "TravelTime.h"
#include "WaveType.h"

struct Layer
{
    Layer() :
        depth(0), speed(0, 0)
    {}

    Layer(float depth, TravelTime speed) :
        depth(depth), speed(speed)
    {}

    Layer(float depth, float pWaveSpeed, float sWaveSpeed) :
        depth(depth), speed(pWaveSpeed, sWaveSpeed)
    {}

    float getSpeedByWaveType(WaveType type)
    {
        switch (type) {
            case WaveType::pWave:
                return speed.pWave;
            case WaveType::sWave:
                return speed.sWave;
        }
        return 0.0f;
    }


    float depth;
    TravelTime speed;

    bool operator<(const Layer& right) const;

    bool operator<=(const Layer& right) const;

    bool operator>(const Layer& right) const;

    bool operator>=(const Layer& right) const;

    bool operator==(const Layer& right) const;

    bool operator!=(const Layer& right) const;

};

class Crustal
{
    public:
        //no copies
        Crustal(const Crustal& other)            = delete;
        Crustal& operator=(const Crustal& right) = delete;

        static Crustal& getInstance();

        void addLayer(Layer layer);
        void addLayer(float depth, float pWaveSpeed, float sWaveSpeed);
        Layer getLayer(float depth) const;
        Layer getLayer(float depth, size_t& index) const;
        std::vector<Layer>& getLayers();

        float getThickness(size_t layerIndex) const;


        float getDwa() const
        {
            return 0.01f;
        }

        size_t layersCount() const
        {
            return layersCount_;
        }

    private:
        Crustal();
        void sortLayers();
        void computeThickness();
        static std::unique_ptr<Crustal> instance_;
        size_t layersCount_;
        std::vector<Layer> layers_;
        std::vector<float> thickness_;
        /* crustal.dat
         Crustal model               Novotny et al., BSSA 2001
        number of layers
           3
        Parameters of the layers
        depth of layer top(km)   Vp(km/s)    Vs(km/s)    Rho(g/cm**3)    Qp     Qs
              0.                  1.30        0.75        2.160         300.   300.
           0.075                  2.30        1.33        2.160         300.   300.
           0.375                  4.20        2.43        2.560         300.   300.
        ***************************************************************************
         */
};


#endif /* CRUSTAL_H */

