#ifndef MATHCONSTANS_H
#define MATHCONSTANS_H

namespace math_constants {
    static const float PI     = 3.1415927f;
    static const float TWO_PI = 6.2831853f;
}

#endif /* MATHCONSTANS_H */

