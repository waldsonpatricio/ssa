#ifndef PARALLELSSA_H
#define PARALLELSSA_H

#include <memory>
#include <string>
#include <fstream>

#include <thrust/host_vector.h>

#include "TravelTime.h"

class SSA;
class Crustal;

class ParallelSSA {
    public:
        ParallelSSA(SSA& ssa, float otIncrement, float samplingInterval, std::string cacheFile);
        std::unique_ptr<thrust::host_vector<float>> compute(
            Crustal& crustal, size_t otAmount, size_t firstOt,
            bool isLast, bool total, size_t securityMargin
        );

    private:
        void compute_travel_times(Crustal& crustal);
        void check_cache();
        void read_from_cache(std::ifstream& file);
        void write_to_cache();

        SSA& ssa_;
        thrust::host_vector<float> values_;
        thrust::host_vector<size_t> offsets_;//offset distance of each station to each point in grid(x,y,z)
        bool travelTimesComputed_;
        float otIncrement_;
        float samplingInterval_;
        std::string cacheFile_;

        const size_t sizeof_cache_header = 8;
};

#endif /* PARALLELSSA_H */

