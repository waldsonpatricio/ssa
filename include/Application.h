#ifndef APPLICATION_H
#define APPLICATION_H

#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>

#include "Crustal.h"
#include "Opener.h"
#include "SSA.h"
#include "SSAResult.h"
#include "Station.h"
#include "TimeFilter.h"
#include "functions.h"

class Application {
public:
  static std::string filenamePlaceholder;

  Application(std::string stationsFile,
              std::string stationFileTemplate, // parameter {name} will be
                                               // replaced by station name
              std::string cacheFile);
  Application(const Application &orig) = delete;
  Application &operator=(const Application &right) = delete;

  SSAResult run(Crustal &crustal, float originTime, float samplingInterval = 0.005f);

  void prepare();

  SSA &getSSA();
  Station* getStationByName(const std::string& name);
  std::string getStationDataFilename(const Station &station);
  void loadTravelTimesFromFile(const std::string& filename);

private:
  void checkCache();
  void writeToCache();
  void readFromCache(std::ifstream &file);
  void populateStations();
  SSA ssa_;
  double dataPower_;
  std::string stationFileTemplate_;
  std::string cacheFile_;
  bool isFirstRun_;
  std::unordered_map<std::string, Station*> stationsMap_;

  const size_t sizeof_size_field = 8;
};

#endif /* APPLICATION_H */
