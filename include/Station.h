#ifndef STATION_H
#define STATION_H

#include <string>
#include <functional>
#include <memory>
#include <vector>
#include <stdexcept>
#include <limits>
#include <unordered_map>
#include <cmath>

#include "functions.h"
#include "TravelTime.h"

class Crustal;
struct Layer;

struct Station
{
    friend class Application;
    struct Indexes {
        size_t pWave;
        size_t sWave;
        size_t psWave;

        Indexes() :
        pWave(-1), sWave(-1), psWave(-1)
        {}

        Indexes(size_t p, size_t s, size_t ps) :
        pWave(p), sWave(s), psWave(ps)
        {}
    };

    struct Log {
        using dataType = float;

        dataType otTime;
        dataType pWave;
        dataType sWave;
        dataType psWave;

        dataType normalizedPWave;
        dataType normalizedSWave;
        dataType normalizedPsWave;

        Log() :
            otTime(0), pWave(0), sWave(0), psWave(0), normalizedPWave(0), normalizedSWave(0), normalizedPsWave(0)
        {}

        Log(dataType p, dataType s, dataType ps) :
            otTime(0), pWave(p), sWave(s), psWave(ps),normalizedPWave(0), normalizedSWave(0), normalizedPsWave(0)
        {}

        Log(const Log& other) :
            otTime(other.otTime), pWave(other.pWave), sWave(other.sWave), psWave(other.psWave), normalizedPWave(other.normalizedPWave), normalizedSWave(other.normalizedSWave), normalizedPsWave(other.normalizedPsWave)
        {}

        dataType& at(unsigned int index)
        {
            if (index == 0) {
                return sWave;
            }
            if (index == 1) {
                return psWave;
            }

            if (index == 2) {
                return pWave;
            }

            throw std::out_of_range{std::string{"Index must be 0, 1, or 2."}};
        }

        dataType& normalizedAt(unsigned int index)
        {
            if (index == 0) {
                return normalizedSWave;
            }
            if (index == 1) {
                return normalizedPsWave;
            }

            if (index == 2) {
                return normalizedPWave;
            }

            throw std::out_of_range{std::string{"Index must be 0, 1, or 2."}};
        }

        Log& operator=(const Log& right)
        {
            if (this != &right) {
                //this->time   = right.time;
                this->pWave  = right.pWave;
                this->sWave  = right.sWave;
                this->psWave = right.psWave;
                this->normalizedPWave  = right.normalizedPWave;
                this->normalizedSWave  = right.normalizedSWave;
                this->normalizedPsWave = right.normalizedPsWave;
            }

            return *this;
        }

        Log operator*(const Log& right) const
        {
            Log result(*this);
            result *= right;
            return result;
        }

        Log& operator*=(const Log& right)
        {
            sWave  *= right.sWave;
            pWave  *= right.pWave;
            psWave *= right.psWave;

            normalizedPWave  *= right.normalizedPWave;
            normalizedSWave  *= right.normalizedSWave;
            normalizedPsWave *= right.normalizedPsWave;

            return *this;
        }

        Log operator+(const Log& right) const
        {
            Log result(*this);
            result += right;
            return result;
        }


        Log& operator+=(const Log& right)
        {
            sWave  += right.sWave;
            pWave  += right.pWave;
            psWave += right.psWave;

            normalizedPWave  += right.normalizedPWave;
            normalizedSWave  += right.normalizedSWave;
            normalizedPsWave += right.normalizedPsWave;

            return *this;
        }

        Log operator-(const Log& right) const
        {
            Log result(*this);
            result -= right;
            return result;
        }


        Log& operator-=(const Log& right)
        {
            sWave  -= right.sWave;
            pWave  -= right.pWave;
            psWave -= right.psWave;

            normalizedPWave  -= right.normalizedPWave;
            normalizedSWave  -= right.normalizedSWave;
            normalizedPsWave -= right.normalizedPsWave;

            return *this;
        }

        Log operator/(const Log& right) const
        {
            Log result(*this);
            result /= right;
            return result;
        }

        Log& operator/=(const Log& right)
        {
            sWave  /= right.sWave;
            psWave /= right.psWave;
            pWave  /= right.pWave;

            normalizedPWave  /= right.normalizedPWave;
            normalizedSWave  /= right.normalizedSWave;
            normalizedPsWave /= right.normalizedPsWave;

            return *this;
        }

        Log operator*(float right) const
        {
            Log result(*this);
            result *= right;
            return result;
        }

        Log& operator*=(float right)
        {
            sWave  *= right;
            pWave  *= right;
            psWave *= right;

            normalizedPWave  *= right;
            normalizedSWave  *= right;
            normalizedPsWave *= right;

            return *this;
        }

        Log operator+(float right) const
        {
            Log result(*this);
            result += right;
            return result;
        }


        Log& operator+=(float right)
        {
            sWave  += right;
            pWave  += right;
            psWave += right;

            normalizedPWave  += right;
            normalizedSWave  += right;
            normalizedPsWave += right;

            return *this;
        }

        Log operator-(float right) const
        {
            Log result(*this);
            result -= right;
            return result;
        }


        Log& operator-=(float right)
        {
            sWave  -= right;
            pWave  -= right;
            psWave -= right;

            normalizedPWave  -= right;
            normalizedSWave  -= right;
            normalizedPsWave -= right;

            return *this;
        }

        Log operator/(float right) const
        {
            Log result(*this);
            result /= right;
            return result;
        }

        Log& operator/=(float right)
        {
            sWave  /= right;
            psWave /= right;
            pWave  /= right;

            normalizedPWave  /= right;
            normalizedSWave  /= right;
            normalizedPsWave /= right;

            return *this;
        }
    };

    struct CacheCoord3D {
        float x;
        float y;
        float z;

        CacheCoord3D(float x_, float y_, float z_) :
            x(x_), y(y_), z(z_) {}

        bool operator==(const CacheCoord3D& right) const
        {
            return x == right.x && y == right.y && z == right.z;
        }

        size_t operator() (const CacheCoord3D& coord) const
        {
            size_t h1 = std::hash<float>()(coord.x);
            size_t h2 = std::hash<float>()(coord.y);
            size_t h3 = std::hash<float>()(coord.z);

            return (h1 ^ (h2 << 1)) ^ h3;
        }
    };

    std::string name;
    float x;
    float y;
    float z;
    bool active;
    float weightP;
    float weightPS;
    float weightS;

    //valores máximos e seus índices
    Log max;
    Indexes maxIndexes;

    Station() :
        name(""),
        x(0),
        y(0),
        z(0),
        active(true),
        weightP(1.0f),
        weightPS(1.0f),
        weightS(1.0f),
        max(-std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity()),
        maxIndexes(),
        logs_(),
        travelTimeCache_()
    {}

    Station(const Station& other) = delete;
    Station& operator=(const Station& right) = delete;

    void setTravelTime(TravelTime& travelTime, const WaveType& type, float time);
    long getPositionHash(float x, float y, float z);
    void cachePositionTravelTime(float x, float y, float z, float pWave, float sWave);
    void cachePositionTravelTime(long hash, float pWave, float sWave);

    int getPWeight() const
    {
        return active ? weightP : 0;
    }

    int getPSWeight() const
    {
        return active ? weightPS : 0;
    }

    int getSWeight() const
    {
        return active ? weightS : 0;
    }

    std::string getName() const {
        return name;
    }

    void addLog(float time, float sWave, float psWave, float pWave);
    void addLog(float time, float sWave, float psWave, float pWave, float normalizedSWave, float normalizedPsWave, float normalizedPWave);
    void normalize();
    void normalize(size_t noSamplesAverage);
    void normalize(size_t noSamplesAverage, size_t offset);

    TravelTime computeTravelTimeToPoint(Crustal& crustal, float x, float y, float z);
    TravelTime computeTravelTimeToPoint(
        Layer& currentLayer,
        size_t currentLayerIndex,
        std::vector<Layer> layers,
        size_t layerCount,
        Crustal& crustal,
        float x, float y, float z
    );

    std::vector<Log>& getLogs()
    {
        return logs_;
    }

    private:
        std::vector<Log> logs_;
        std::unordered_map<long, std::unique_ptr<TravelTime>> travelTimeCache_;

        void help1(float p, float vmax, float v, float d, float& td, float& tt);
        void help2(float p, float vmax, float v, float d, float& td, float& tt);
        void help3(float p, float v, float d, float& td, float& tt);
};

Station::Log operator+(float value, const Station::Log& log);
Station::Log operator-(float value, const Station::Log& log);
Station::Log operator*(float value, const Station::Log& log);
Station::Log operator/(float value, const Station::Log& log);


#endif /* STATION_H */

