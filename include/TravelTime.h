#ifndef TRAVELTIME_H
#define TRAVELTIME_H

#include "WaveType.h"

struct TravelTime
{
    float sWave;
    float pWave;

    TravelTime() :
        sWave(0),
        pWave(0)
    {}

    TravelTime(float p, float s) :
        sWave(s), pWave(p)
    {}

    TravelTime(const TravelTime& other) :
    sWave(other.sWave), pWave(other.pWave)
    {}

    float getSpeedByWaveType(WaveType type)
    {
        switch (type) {
            case WaveType::pWave:
                return pWave;
            case WaveType::sWave:
                return sWave;
        }
        return 0;
    }

    TravelTime& operator=(const TravelTime& right)
    {
        if (this == &right) {
            return *this;
        }

        this->pWave = right.pWave;
        this->sWave = right.sWave;

        return *this;
    }

    TravelTime operator*(const TravelTime& right) const
    {
        TravelTime result(*this);
        result *= right;
        return result;
    }

    TravelTime& operator*=(const TravelTime& right)
    {
        this->pWave *= right.pWave;
        this->sWave *= right.sWave;
        return *this;
    }

    TravelTime operator+(const TravelTime& right) const
    {
        TravelTime result(*this);
        result += right;
        return result;
    }

    TravelTime& operator+=(const TravelTime& right)
    {
        this->pWave += right.pWave;
        this->sWave += right.sWave;

        return *this;
    }

    TravelTime operator-(const TravelTime& right) const
    {
        TravelTime result(*this);
        result -= right;
        return result;
    }

    TravelTime& operator-=(const TravelTime& right)
    {
        this->pWave -= right.pWave;
        this->sWave -= right.sWave;

        return *this;
    }

    TravelTime operator/(const TravelTime& right) const
    {
        TravelTime result(*this);
        result /= right;
        return result;
    }

    TravelTime& operator/=(const TravelTime& right)
    {
        this->pWave /= right.pWave;
        this->sWave /= right.sWave;

        return *this;
    }


};


#endif /* TRAVELTIME_H */

