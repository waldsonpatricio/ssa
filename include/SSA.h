#ifndef SSA_H
#define SSA_H

#include <chrono>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <unordered_map>

#include <iostream>

#include "Extent.h"
#include "Station.h"
#include "functions.h"
#include "TimeFilter.h"
#include "SSAResult.h"

class SSA {
    public:
        SSA();
        ~SSA();

        void addStation(std::string name, float x, float y, float z);
        void addStation(std::unique_ptr<Station> station);

        void setXExtent(const Extent& xExtent);
        void setYExtent(const Extent& yExtent);
        void setZExtent(const Extent& zExtent);

        void setXExtent(float min, float max, float step);
        void setYExtent(float min, float max, float step);
        void setZExtent(float min, float max, float step);

        const Extent& getXExtent() const;
        const Extent& getYExtent() const;
        const Extent& getZExtent() const;

        std::size_t getNumberOfSamples() const;
        std::size_t getNumberOfStations() const;

        std::vector<std::unique_ptr<Station>>& getStations();
        Station& getStationAt(unsigned int pos);

        unsigned int computeNumberOfActiveStations() const;
        SSAResult run(Crustal& crustal, float samplingInterval, float originTime);

        size_t getIndexOfPosition(float x, float y, float z) const;

        float getBrightnessLimit() const
        {
            return brightnessLimit_;
        }

        void setBrightnessLimit(float brightnessLimit)
        {
            this->brightnessLimit_ = brightnessLimit;
        }


        // copy not allowed
        SSA(const SSA& orig) = delete;
        SSA& operator=(const SSA& right) = delete;



    private:

        bool enablePicker_;
        Extent xExtent_;
        Extent yExtent_;
        Extent zExtent_;
        float brightnessLimit_;
        std::vector<std::unique_ptr<Station>> stations_;
};

#endif /* SSA_H */

