#include "functions.h"



float warp(float frequency, float samplingInteval) 
{
    using namespace math_constants;
    
    auto angle = TWO_PI * frequency * samplingInteval / 2.0f;
    auto warp  = 2.0f * std::tan(angle) / samplingInteval;
    return warp / TWO_PI;
}


float chebyshevPassband(float stopBandAttenuation, float transitionBandwidth, unsigned int order)
{
    auto omegaR = 1.0f + transitionBandwidth;
    auto alpha  = std::pow((omegaR + std::sqrt(std::pow(omegaR, 2) - 1.0f)), order);
    auto g      = (std::pow(alpha, 2) + 1) / 2.0f / alpha;
    
    return std::sqrt(std::pow(stopBandAttenuation, 2) - 1.0f) / g;
}

float chebyshevRipple(float passband)
{
    return 1.0f / std::sqrt(1.0f + std::pow(passband, 2));
}