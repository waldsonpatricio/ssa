#include "TimeFilter.h"

TimeFilter::TimeFilter(
    Algorithm algorithm, Type type, PassType passType, 
    float transitionBandwidth, float attenuationFactor, unsigned int order,
    float lowFrequencyCutOff, float highFrequencyCutOff, float samplingInterval
)  :
    type_(type),
    algorithm_(algorithm),
    passType_(passType),
    transitionBandwidth_(transitionBandwidth),
    attenuationFactor_(attenuationFactor),
    order_(order),
    lowFrequencyCutOff_(lowFrequencyCutOff),
    highFrequencyCutOff_(highFrequencyCutOff),
    samplingInterval_(samplingInterval),
    coefficients_()
{
    using std::vector;
    using std::unique_ptr;
    using std::complex;
    
    auto roots   = vector<Root>{};
    auto zeros   = vector<complex<float>>{};
    auto dcValue = 0.0f;
    
    switch(algorithm_) {
        case Algorithm::Bessel:
            roots = setupBessel(dcValue);
            break;
        case Algorithm::Butterworth:
            roots = setupButterWorth(dcValue);
            break;
        case Algorithm::ChebyshevTypeI:
            roots = setupChebyshevTypeI(dcValue);
            break;
        case Algorithm::ChebyshevTypeII:
            roots = setupChebyshevTypeII(dcValue, zeros);
            break;
    }
    
    switch (type_) {
        case Type::BandPass: {
                auto lowFrequency  = warp(lowFrequencyCutOff_ * samplingInterval_ / 2.0f, 2.0f);
                auto highFrequency = warp(highFrequencyCutOff_ * samplingInterval_ / 2.0f, 2.0f);
                coefficients_      = lowPassToBandPass(roots, zeros, dcValue, lowFrequency, highFrequency);
            }
            break;
        case Type::BandReject: {
                auto lowFrequency  = warp(lowFrequencyCutOff_ * samplingInterval_ / 2.0f, 2.0f);
                auto highFrequency = warp(highFrequencyCutOff_ * samplingInterval_ / 2.0f, 2.0f);
                coefficients_      = lowPassToBandReject(roots, zeros, dcValue, lowFrequency, highFrequency);
            }
            break;
        case Type::HighPass: {
                auto lowFrequency = warp(lowFrequencyCutOff_ * samplingInterval_ / 2.0f, 2.0f);
                coefficients_     = lowPassToHighPass(roots, zeros, dcValue);
                cutoffs(coefficients_, lowFrequency);
            }
            break;
        case Type::LowPass: {
                auto highFrequency = warp(highFrequencyCutOff_ * samplingInterval_ / 2.0f, 2.0f);
                coefficients_      = lowPass(roots, zeros, dcValue);
                cutoffs(coefficients_, highFrequency);
            }
            break;
    }
    bilinear(coefficients_);
}

float TimeFilter::operator()(std::vector<Station::Log>& logs) const
{
    auto rr = 0.0;
    auto forwardOnly = passType_ == PassType::Forward;
    auto loopMax = static_cast<int>(coefficients_.size() / 3);
    auto current = 0;
    for (auto i = size_t{0}; i < coefficients_.size(); i += 3) {
        ++current;
        
        auto x1 = Station::Log{};
        auto x2 = Station::Log{};
        auto y1 = Station::Log{};
        auto y2 = Station::Log{};
        
        auto b0 = coefficients_[i].numerator;
        auto b1 = coefficients_[i+1].numerator;
        auto b2 = coefficients_[i+2].numerator;
        
        auto a1 = coefficients_[i+1].denominator;
        auto a2 = coefficients_[i+2].denominator;
        
        for (auto& l : logs) {
            auto output =  b0 * l + b1 * x1 + b2 * x2;
            output     -= (a1 * y1 + a2 * y2);
            
            y2          = y1;
            y1          = output;
            x2          = x1;
            x1          = l;
            l          = output;
            
            if (forwardOnly && current == loopMax) {//final value
                rr += l.pWave * l.pWave;
                rr += l.sWave * l.sWave;
                rr += l.psWave * l.psWave;
            }
        }
    }
    
    if (!forwardOnly) {
        for (auto i = size_t{0}; i < coefficients_.size(); i += 3) {
            auto x1 = Station::Log{};
            auto x2 = Station::Log{};
            auto y1 = Station::Log{};
            auto y2 = Station::Log{};

            auto b0 = coefficients_[i].numerator;
            auto b1 = coefficients_[i+1].numerator;
            auto b2 = coefficients_[i+2].numerator;

            auto a1 = coefficients_[i+1].denominator;
            auto a2 = coefficients_[i+2].denominator;

            for (auto& l : logs) {
                auto output =  b0 * l + b1 * x1 + b2 * x2;
                output      = output - (a1 * y1) + a2 * y2;
                y2          = y1;
                y1          = output;
                x2          = x1;
                x1          = l;
                l          = output;
                
                if (current == loopMax) {
                    rr += l.pWave * l.pWave;
                    rr += l.sWave * l.sWave;
                    rr += l.psWave * l.psWave;
                }
                
            }
        }
    }
    
    return rr * samplingInterval_;
}

void TimeFilter::bilinear(std::vector<Coefficient>& coefficients) 
{
    for (auto i = size_t{0}; i < coefficients.size(); i += 3) {
        
        auto a0 = coefficients[i].denominator;
        auto a1 = coefficients[i+1].denominator;
        auto a2 = coefficients[i+2].denominator;
        
        auto scale = a2 + a1 + a0;
        
        coefficients[i].denominator   = 1.0f;
        coefficients[i+1].denominator = 2.0f * (a0 - a2) / scale;
        coefficients[i+2].denominator = (a2 - a1 + a0) / scale;
                
        a0 = coefficients[i].numerator;
        a1 = coefficients[i+1].numerator;
        a2 = coefficients[i+2].numerator;
                
        coefficients[i].numerator   = (a2 + a1 + a0) / scale;
        coefficients[i+1].numerator = 2.0f * (a0 - a2) / scale;
        coefficients[i+2].numerator = (a2 - a1 + a0) / scale;
    }
}


std::vector<Coefficient> TimeFilter::lowPassToHighPass(const std::vector<Root>& roots, const std::vector<std::complex<float> > zeros, float dcValue) 
{
    using std::vector;
    
    auto coefficients = vector<Coefficient>{};
    
    
    for (auto i = size_t{0}; i < roots.size(); ++i) {
        auto& root = roots[i];
        switch (root.type) {
            case Root::Type::ComplexConjugatePoleZeroPair: {
                    auto& zero = zeros[i];
                    auto scale = (root.complex * std::conj(root.complex)).real() / (zero * std::conj(zero)).real();
                    addCoefficient(
                        coefficients,
                        scale,
                        1.0f
                    );
                    addCoefficient(
                        coefficients,
                        -2.0f * zero.real() * scale,
                        -2.0f * root.complex.real()
                    );
                    addCoefficient(
                        coefficients,
                        (zero * std::conj(zero)).real() * scale,
                        (root.complex * std::conj(root.complex)).real()
                    );
                }
                break;
            case Root::Type::ComplexConjugatePolePair: {
                    auto scale = (root.complex * std::conj(root.complex)).real();
                    addCoefficient(
                        coefficients,
                        0.0f,
                        1.0f
                    );
                    addCoefficient(
                        coefficients,
                        0.0f,
                        -2.0f * root.complex.real()
                    );
                    addCoefficient(
                        coefficients,
                        scale,
                        (root.complex * std::conj(root.complex)).real()
                    );
                    
                }
                break;
            case Root::Type::SingleRealPole: {
                    auto scale = -(root.complex.real());
                    addCoefficient(
                        coefficients,
                        0.0f,
                        1.0f
                    );
                    
                    addCoefficient(
                        coefficients,
                        scale,
                        scale
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        0.0f
                    );
                }
                break;
        }
    }
    
    coefficients[0].numerator *= dcValue;
    coefficients[1].numerator *= dcValue;
    coefficients[2].numerator *= dcValue;
    
    return coefficients;
}


void TimeFilter::cutoffs(std::vector<Coefficient>& coefficients, float cutoffFrequency) 
{
    auto scale = math_constants::TWO_PI * cutoffFrequency;
    for (auto i = size_t{0}; i < coefficients.size(); i+= 3) {
        coefficients[i + 1].numerator /= scale;
        coefficients[i + 2].numerator /= scale*scale;
        
        coefficients[i + 1].denominator /= scale;
        coefficients[i + 2].denominator /= scale*scale;
    }
}


std::vector<Coefficient> TimeFilter::lowPass(const std::vector<Root>& roots, const std::vector<std::complex<float> > zeros, float dcValue) 
{
    using std::vector;
    using namespace math_constants;
    
    auto coefficients = vector<Coefficient>{};
    
    
    for (auto i = size_t{0}; i < roots.size(); ++i) {
        auto& root = roots[i];
        switch (root.type) {
            case Root::Type::ComplexConjugatePoleZeroPair: {
                    auto scale = (root.complex * std::conj(root.complex)).real() / (zeros[i] * std::conj(zeros[i])).real();
                    addCoefficient(
                        coefficients,
                        (zeros[i] * std::conj(zeros[i])).real() * scale,
                        (root.complex * std::conj(root.complex)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        -2.0f * zeros[i].real() * scale,
                        -2.0f * root.complex.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        scale,
                        1.0f
                    );
                }
                break;
            case Root::Type::ComplexConjugatePolePair: {
                    auto scale = (root.complex * std::conj(root.complex)).real();
                    addCoefficient(
                        coefficients,
                        scale,
                        scale    
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        -2.0f * root.complex.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        1.0f
                    );
                }
                break;
            case Root::Type::SingleRealPole: {
                    auto scale = -(root.complex.real());
                    addCoefficient(
                        coefficients,
                        scale,
                        scale    
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        1.0f
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        0.0f
                    );
                }
                break;
        }
    }
    
    coefficients[0].numerator *= dcValue;
    coefficients[1].numerator *= dcValue;
    coefficients[2].numerator *= dcValue;
    
    
    return coefficients;
}


std::vector<Coefficient> TimeFilter::lowPassToBandPass(
    const std::vector<Root>& roots, const std::vector<std::complex<float>> zeros, 
    float dcValue, float lowFrequencyCutoff, float highFrequencyCutoff
) {
    using std::vector;
    using namespace math_constants;
    
    auto coefficients = vector<Coefficient>{};
    
    auto a = TWO_PI * TWO_PI * lowFrequencyCutoff * highFrequencyCutoff;
    auto b = TWO_PI * (highFrequencyCutoff - lowFrequencyCutoff);
    
    for (auto i = size_t{0}; i < roots.size(); ++i) {
        auto& root = roots[i];
        
        switch (root.type) {
            case Root::Type::ComplexConjugatePoleZeroPair: {
                    auto temp = std::pow(b * zeros[i], 2.0f) - 4.0f * a;
                    temp      = std::sqrt(temp);

                    auto z1 = 0.5f * (b * zeros[i] + temp);
                    auto z2 = 0.5f * (b * zeros[i] - temp);

                    temp =  std::pow(b * root.complex, 2.0f) - 4.0f * a;
                    temp = std::sqrt(temp);

                    auto p1 = 0.5f * (b * root.complex + temp);
                    auto p2 = 0.5f * (b * root.complex - temp);
                    
                    addCoefficient(
                        coefficients,
                        (z1 * std::conj(z1)).real(),
                        (p1 * std::conj(p1)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        -2.0f * z1.real(),
                        -2.0f * p1.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        1.0f,
                        1.0f
                    );
                    
                    addCoefficient(
                        coefficients,
                        (z2 * std::conj(z2)).real(),
                        (p2 * std::conj(p2)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        -2.0f * z2.real(),
                        -2.0f * p2.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        1.0f,
                        1.0f
                    );
                }
                break;
            case Root::Type::ComplexConjugatePolePair: {
                    auto temp = std::pow(b * root.complex, 2.0f) - 4.0f * a;
                    temp      = std::sqrt(temp);
                    
                    auto p1 = 0.5f * (b * root.complex + temp);
                    auto p2 = 0.5f * (b * root.complex - temp);
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        (p1 * std::conj(p1)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        b,
                        -2.0f * p1.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        1.0f
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        (p2 * std::conj(p2)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        b,
                        -2.0f * p2.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        1.0f
                    );
                }
                break;
            case Root::Type::SingleRealPole: {
                    addCoefficient(
                        coefficients,
                        0.0f,
                        a
                    );
                    
                    addCoefficient(
                        coefficients,
                        b,
                        -b * root.complex.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        0,
                        1.0f
                    );
                }
                break;
        }
    }
    
    auto s = std::complex<float>{0.0f, std::sqrt(a)};
    auto h = std::complex<float>{1.0f, 0.0f};
    
    for (auto i = size_t{0}; i < coefficients.size(); i+= 3) {
        auto numerator   = (coefficients[i + 2].numerator * s + coefficients[i+1].numerator) * s + coefficients[i].numerator;
        auto denominator = (coefficients[i + 2].denominator * s + coefficients[i+1].denominator) * s + coefficients[i].denominator;
        h = h * (numerator / denominator);
    }
    
    auto scale = dcValue / std::sqrt(h.real() * std::conj(h));
    
    /*for (auto& c : coefficients) {
        c.numerator *= scale.real();
    }*/
    coefficients[0].numerator *= scale.real();
    coefficients[1].numerator *= scale.real();
    coefficients[2].numerator *= scale.real();
    
    return coefficients;
}

std::vector<Coefficient> TimeFilter::lowPassToBandReject(
    const std::vector<Root>& roots, const std::vector<std::complex<float>> zeros, 
    float dcValue, float lowFrequencyCutoff, float highFrequencyCutoff
) {
    using std::vector;
    using namespace math_constants;
    
    auto coefficients = vector<Coefficient>{};
    
    auto a = TWO_PI * TWO_PI * lowFrequencyCutoff * highFrequencyCutoff;
    auto b = TWO_PI * (highFrequencyCutoff - lowFrequencyCutoff);
    
    for (auto i = size_t{0}; i < roots.size(); ++i) {
        auto& root = roots[i];
        
        switch (root.type) {
            case Root::Type::ComplexConjugatePoleZeroPair: {
                    auto inv  = 1.0f / zeros[i];
                    auto temp = std::pow(b * inv, 2.0f) - 4.0f * a;
                    temp      = std::sqrt(temp);
                    
                    auto z1 = 0.5f * (b * inv + temp);
                    auto z2 = 0.5f * (b * inv - temp);
                    
                    inv  = 1.0f / root.complex;
                    temp = std::pow(b * inv, 2.0f) - 4.0f * a; 
                    temp = std::sqrt(temp);
                    
                    auto p1 = 0.5f * (b * inv + temp);
                    auto p2 = 0.5f * (b * inv - temp);
                    
                    addCoefficient(
                        coefficients,
                        (z1 * std::conj(z1)).real(),
                        (p1 * std::conj(p1)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        -2.0f * z1.real(),
                        -2.0f * p1.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        1.0f,
                        1.0f
                    );
                    
                    addCoefficient(
                        coefficients,
                        (z2 * std::conj(z2)).real(),
                        (p2 * std::conj(p2)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        -2.0f * z2.real(),
                        -2.0f * p2.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        1.0f,
                        1.0f
                    );
                }
                break;
            case Root::Type::ComplexConjugatePolePair: {
                    auto inv  = 1.0f / root.complex;
                    auto temp = std::pow(b * inv, 2.0f) - 4.0f * a; 
                    temp      = std::sqrt(temp);
                    
                    auto p1 = 0.5f * (b * inv + temp);
                    auto p2 = 0.5f * (b * inv - temp);
                    
                    addCoefficient(
                        coefficients,
                        a,
                        (p1 * std::conj(p1)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        -2.0f * p1.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        1.0f,
                        1.0f
                    );
                    
                    addCoefficient(
                        coefficients,
                        a,
                        (p2 * std::conj(p2)).real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        -2.0f * p2.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        1.0f,
                        1.0f
                    );
                }
                break;
            case Root::Type::SingleRealPole: {
                    addCoefficient(
                        coefficients,
                        a,
                        -a * root.complex.real()
                    );
                    
                    addCoefficient(
                        coefficients,
                        0.0f,
                        b
                    );
                    
                    addCoefficient(
                        coefficients,
                        1.0f,
                        -(root.complex.real())
                    );
                }
                break;
        }
    }
    
    auto h = 1.0f;
    
    for (auto i = size_t{0}; i < coefficients.size(); i += 3) {
        h *= coefficients[i].numerator / coefficients[i].denominator;
    }
    
    auto scale = dcValue / std::abs(h);
    
    coefficients[0].numerator *= scale;
    coefficients[1].numerator *= scale;
    coefficients[2].numerator *= scale;
    
    
    return coefficients;
}

std::vector<Root> TimeFilter::setupBessel(float& dcValue) 
{
    using std::vector;
    using std::unique_ptr;
    
    auto roots = vector<Root>{};
    
    switch(order_) {
        case 1:
            addRoot(roots, -1.0f, 0.0f, Root::Type::SingleRealPole);
            break;
        case 2:
            addRoot(roots, -1.1016013f, 0.6360098f, Root::Type::ComplexConjugatePolePair);
            break;
        case 3:
            addRoot(roots, -1.0474091f, 0.9992645f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.3226758f, 0.0f, Root::Type::SingleRealPole);
            break;
        case 4:
            addRoot(roots, -0.9952088f, 1.2571058f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.3700679f, 0.4102497f, Root::Type::ComplexConjugatePolePair);
            break;
        case 5:
            addRoot(roots, -0.9576766f, 1.4711244f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.3808774f, 0.7179096f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.5023160f, 0.0f, Root::Type::SingleRealPole);
            break;
        case 6:
            addRoot(roots, -0.9306565f, 1.6618633f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.3818581f, 0.9714719f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.5714904f, 0.3208964f, Root::Type::ComplexConjugatePolePair);
            break;
        case 7:
            addRoot(roots, -0.9098678f, 1.8364514f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.3789032, 1.1915667f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.6120388f, 0.5892445f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.6843682f, 0.0f, Root::Type::SingleRealPole);
            break;
        case 8:
            addRoot(roots, -0.8928710f, 1.9983286f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.3738431, 1.3883585f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.6369417f, 0.8227968f, Root::Type::ComplexConjugatePolePair);
            addRoot(roots, -1.7574108f, 0.2728679f, Root::Type::ComplexConjugatePolePair);
            break;
        default:
            throw std::invalid_argument{
                std::string{"Invalid order argument. Bessel's filter order should between 1 and 8."}
            };
    }
    
    dcValue = 1.0f;
    
    return roots;
}

std::vector<Root> TimeFilter::setupButterWorth(float& dcValue) 
{
    using std::vector;
    using std::unique_ptr;
    
    auto roots = vector<Root>{};
    auto half  = trunc(order_ / 2.0f);
    
    if (2.0f * half < order_) {
        roots.push_back(createRoot(-1.0f, 0.0f, Root::Type::SingleRealPole));
    }
    
    for (auto k = 0; k < half; ++k) {
        auto angle = math_constants::PI * (0.5f + (2.0f * (k+1)-1) / static_cast<float>(2.0f * order_));
        roots.push_back(
            createRoot(
                std::cos(angle), 
                std::sin(angle), 
                Root::Type::ComplexConjugatePolePair
            )
        );
    }
    
    dcValue = 1.0f;
    
    return roots;
}

void TimeFilter::addRoot(std::vector<Root>& roots, float real, float imaginary, Root::Type type) 
{
    roots.push_back(createRoot(real, imaginary, type));
}

inline Root TimeFilter::createRoot(float real, float imaginary, Root::Type type) 
{
    auto root = Root{};
    
    root.complex.real(real);
    root.complex.imag(imaginary);
    root.type = type;
    
    return root;
}

void TimeFilter::addCoefficient(std::vector<Coefficient>& coefficients, float numerator, float denominator) 
{
    coefficients.push_back(createCoefficient(numerator, denominator));
}

Coefficient TimeFilter::createCoefficient(float numerator, float denominator) 
{
    auto c        = Coefficient{};
    c.numerator   = numerator;
    c.denominator = denominator;
    
    return c;
}



std::vector<Root> TimeFilter::setupChebyshevTypeI(float& dcValue) 
{
    using std::vector;
    using std::unique_ptr;
    
    auto roots = vector<Root>{};
    auto half  = trunc(order_ / 2.0f);
    
    auto eps = chebyshevPassband(attenuationFactor_, transitionBandwidth_, order_);
    
    auto gamma = (1.0f + (std::sqrt(1.0f + eps * eps))) / eps;
    gamma = std::log(gamma) / static_cast<float>(order_);
    gamma = std::exp(gamma);
    
    auto s = 0.5f * (gamma - 1.0f/gamma);
    auto c = 0.5f * (gamma + 1.0f/gamma);
    
    for (auto i = 0; i < half; ++i) {
        auto angle = (2 * (i+1) - 1.0f) * math_constants::PI / static_cast<float>(2.0f * order_);
        auto sigma = -s * std::sin(angle);
        auto omega = c * std::cos(angle);
        
        addRoot(roots, sigma, omega, Root::Type::ComplexConjugatePolePair);
    }
    
    if (2.0f * half < order_) {
        addRoot(roots, -s, 0.0f, Root::Type::SingleRealPole);
        dcValue = 1.0f;
    } else {
        dcValue = 1.0f / std::sqrt(1.0f + eps * eps);
    }
    
    return roots;
}

std::vector<Root> TimeFilter::setupChebyshevTypeII(float& dcValue, std::vector<std::complex<float> >& zeros) 
{
    using std::vector;
    using std::unique_ptr;
    
    auto roots  = vector<Root>{};
    auto omegaR = 1.0f + transitionBandwidth_;
    auto half   = trunc(order_ / 2.0f);
    
    auto gamma = (attenuationFactor_ + (std::sqrt(attenuationFactor_ * attenuationFactor_ - 1.0f)));
    gamma = std::log(gamma) / static_cast<float>(order_);
    gamma = std::exp(gamma);
    
    auto s = 0.5f * (gamma - 1.0f/gamma);
    auto c = 0.5f * (gamma + 1.0f/gamma);
    
    for (auto i = 0; i < half; ++i) {
        auto angle = 2.0f * (i+1) - 1.0f * math_constants::PI / static_cast<float>(2.0f * order_);
        auto alpha = -s * std::sin(angle);
        auto beta  = c * std::cos(angle);
        auto denom = alpha * alpha + beta * beta;
        auto sigma = omegaR * alpha / denom;
        auto omega = -omegaR * beta / denom;
        addRoot(roots, sigma, omega, Root::Type::ComplexConjugatePoleZeroPair);
        
        omega = omegaR / std::cos(angle);
        zeros.push_back(std::complex<float>(0.0f, omega));
    }
    
    if (2.0f * half < order_) {
        addRoot(roots, -omegaR/s, 0.0f, Root::Type::SingleRealPole);
    }
    
    dcValue = 1.0f;
    
    
    return roots;
}

inline int TimeFilter::trunc(float value) 
{
    return std::floor(value);
}
