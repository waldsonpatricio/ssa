#include "SSA.h"
#include "Crustal.h"

SSA::SSA() :
    enablePicker_(false),
    xExtent_(),
    yExtent_(),
    zExtent_(),
    brightnessLimit_(0.8f),
    stations_()
{
}

SSA::~SSA()
{
    stations_.clear();
}


void SSA::addStation(std::string name, float x, float y, float z)
{
    auto s    = make_unique<Station>();
    (*s).name = name;
    (*s).x    = x;
    (*s).y    = y;
    (*s).z    = z;
    stations_.push_back(std::move(s));
}

void SSA::addStation(std::unique_ptr<Station> station)
{
    stations_.push_back(std::unique_ptr<Station>{std::move(station)});
}

void SSA::setXExtent(float min, float max, float step)
{
    xExtent_.min  = min;
    xExtent_.max  = max;
    xExtent_.step = step;
}

void SSA::setYExtent(float min, float max, float step)
{
    yExtent_.min  = min;
    yExtent_.max  = max;
    yExtent_.step = step;

}

void SSA::setZExtent(float min, float max, float step)
{
    zExtent_.min  = min;
    zExtent_.max  = max;
    zExtent_.step = step;
}

void SSA::setXExtent(const Extent& xExtent)
{
    xExtent_.min  = xExtent.min;
    xExtent_.max  = xExtent.max;
    xExtent_.step = xExtent.step;
}

void SSA::setYExtent(const Extent& yExtent)
{
    yExtent_.min  = yExtent.min;
    yExtent_.max  = yExtent.max;
    yExtent_.step = yExtent.step;
}

void SSA::setZExtent(const Extent& zExtent)
{
    zExtent_.min  = zExtent.min;
    zExtent_.max  = zExtent.max;
    zExtent_.step = zExtent.step;
}

const Extent& SSA::getXExtent() const
{
    return xExtent_;
}

const Extent& SSA::getYExtent() const
{
    return yExtent_;
}

const Extent& SSA::getZExtent() const
{
    return zExtent_;
}

std::size_t SSA::getNumberOfSamples() const
{
    return xExtent_.getNumberOfSamples() * yExtent_.getNumberOfSamples() *
            zExtent_.getNumberOfSamples();
}

std::size_t SSA::getNumberOfStations() const
{
    return stations_.size();
}

std::vector<std::unique_ptr<Station> >& SSA::getStations()
{
    return stations_;
}


Station& SSA::getStationAt(unsigned int pos)
{
    return *stations_[pos];
}

unsigned int SSA::computeNumberOfActiveStations() const
{
    auto activeStations = 0;
    for (auto& stationPointer : stations_) {
        if ((*stationPointer).active) {
            ++activeStations;
        }
    }
    return activeStations;
}


SSAResult SSA::run(Crustal& crustal, float samplingInterval, float originTime)
{
    //auto tmw  = 0.0f;//TODO: adicionar código para quando o flag picker = 1 (ver linha 415 do main em fortran)
    //auto tmw2 = 0.0f;

    auto brPmax      = -std::numeric_limits<float>::infinity();
    auto brPmaxIndex = 0;


#ifdef SSA_COMPUTE_S_AND_PS
    auto brSmax      = -std::numeric_limits<float>::infinity();;
    auto brSmaxIndex = 0;

    auto brPsMax      = -std::numeric_limits<float>::infinity();;
    auto brPsMaxIndex = 0;
#endif

    auto currentIndex = 0;

    SSAResult result{};

    auto start    = std::chrono::system_clock::now();

    auto& layers      = crustal.getLayers();
    auto layerCount   = crustal.layersCount();

    for (size_t iZ = 0; iZ < getZExtent().getNumberOfSamples(); iZ++) {
        auto z = getZExtent().min + (iZ * getZExtent().step);
        auto currentLayerIndex = size_t{};
        auto currentLayer      = crustal.getLayer(z, currentLayerIndex);


        for (size_t iX = 0; iX < getXExtent().getNumberOfSamples(); iX++) {
            auto x = getXExtent().min + (iX * getXExtent().step);
            for (size_t iY = 0; iY < getYExtent().getNumberOfSamples(); iY++) {
                auto y = getYExtent().min + (iY * getYExtent().step);

                auto stationsUsedP = 0;
                auto stackP        = 0.0f;

#ifdef SSA_COMPUTE_S_AND_PS
                auto stationsUsedS = 0;
                auto stackS        = 0.0f;
#endif

                for (auto& stationPtr : stations_) {
                    auto& station = *stationPtr;

                    if (!station.active) {
                        continue;
                    }
                    auto travelTime = station.computeTravelTimeToPoint(
                        currentLayer,
                        currentLayerIndex,
                        layers,
                        layerCount,
                        crustal,
                        x, y, z
                    );

                    auto pIndex = static_cast<int>((originTime + travelTime.pWave) / samplingInterval);
#ifdef SSA_COMPUTE_S_AND_PS
                    auto sIndex = static_cast<int>((originTime + travelTime.sWave) / samplingInterval);
#endif

                    try {
                        //separated to not mess with stack if throws an exception
                        auto pValue = station.getLogs()[pIndex].pWave * station.getPWeight();
                        stackP     += pValue;
                        ++stationsUsedP;
                    } catch (std::out_of_range&) {
                        std::cout << "out of range";
                    }

#ifdef SSA_COMPUTE_S_AND_PS
                    try {
                        //separated to not mess with stack if throws an exception
                        auto sValue = station.getLogs()[sIndex].sWave  * station.getSWeight();
                        stackS     += sValue;
                        ++stationsUsedS;
                    } catch (std::out_of_range&) {}
#endif
                }

                auto goodP  = 0.0f;
#ifdef SSA_COMPUTE_S_AND_PS
                auto goodS  = 0.0f;
                auto goodPs = 0.0f;
#endif

                if (stationsUsedP > 0) {
                    goodP = stackP / stationsUsedP;
                }

#ifdef SSA_COMPUTE_S_AND_PS
                if (stationsUsedS > 0) {
                    goodS = stackS / stationsUsedS;
                }

                if (stationsUsedP > 0 || stationsUsedS > 0) {
                    auto denominator = static_cast<float>(stationsUsedP + stationsUsedS) / 2.0f;
                    goodPs           = std::sqrt(stackP * stackS) / denominator;
                }
#endif

                if (goodP > brPmax) {
                    brPmax      = goodP;
                    brPmaxIndex = currentIndex;
                }

#ifdef SSA_COMPUTE_S_AND_PS
                if (goodS > brSmax) {
                    brSmax      = goodS;
                    brSmaxIndex = currentIndex;
                }

                if (goodPs > brPsMax) {
                    brPsMax      = goodPs;
                    brPsMaxIndex = currentIndex;
                }
#endif

#ifdef SSA_COMPUTE_S_AND_PS
                result.addInfo(x, y, z, stationsUsedP, stationsUsedS, stackP, stackS, goodP, goodS, goodPs);
#else
                result.addInfo(x, y, z, stationsUsedP, 0, stackP, 0, goodP, 0, 0);
#endif

                ++currentIndex;
            }
        }
    }

    auto brightnessCutoffP  = brightnessLimit_ * brPmax;

#ifdef SSA_COMPUTE_S_AND_PS
    auto brightnessCutoffS  = brightnessLimit_ * brSmax;
    auto brightnessCutoffPs = brightnessLimit_ * brPsMax;
#endif

    auto numP  = 0;
#ifdef SSA_COMPUTE_S_AND_PS
    auto numS  = 0;
    auto numPs = 0;
#endif

    for (auto& infoPtr : result.getInfos()) {
        auto& info = *infoPtr;

        if (info.goodP > brightnessCutoffP) {
            ++numP;
        }

#ifdef SSA_COMPUTE_S_AND_PS
        if (info.goodS > brightnessCutoffS) {
            ++numS;
        }

        if (info.goodPs > brightnessCutoffPs) {
            ++numPs;
        }
#endif
    }

    auto end = std::chrono::system_clock::now();

    result.executionTime_ = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() / 1000.0f;

    result.ratNumP_  = static_cast<float>(numP) / currentIndex;

#ifdef SSA_COMPUTE_S_AND_PS
    result.ratNumS_  = static_cast<float>(numS) / currentIndex;
    result.ratNumPs_ = static_cast<float>(numPs) / currentIndex;
#endif

    result.ratGoodP_  = brPmax  / result.ratNumP_;
#ifdef SSA_COMPUTE_S_AND_PS
    result.ratGoodS_  = brSmax  / result.ratNumS_;
    result.ratGoodPs_ = brPsMax / result.ratNumPs_;
#endif

    result.numP_  = numP;
#ifdef SSA_COMPUTE_S_AND_PS
    result.numS_  = numS;
    result.numPS_ = numPs;
#endif

    result.maximumPWaveBrightnessInfoIndex_  = brPmaxIndex;
#ifdef SSA_COMPUTE_S_AND_PS
    result.maximumSWaveBrightnessInfoIndex_  = brSmaxIndex;
    result.maximumPsWaveBrightnessInfoIndex_ = brPsMaxIndex;
#endif

    return result;
}

size_t SSA::getIndexOfPosition(float x, float y, float z) const
{
    if (
           z < zExtent_.min || z > zExtent_.max
        || x < xExtent_.min || x > xExtent_.max
        || y < yExtent_.min || y > yExtent_.max
    ) {
        /* throw std::out_of_range("Position is out of boundaries."); */
    }
    //SSA::run's loop order:z, x, y
    //auto zRange = zExtent_.max - zExtent_.min;

    //auto totalZ = std::ceil(zRange / zExtent_.step);
    auto totalX = xExtent_.getNumberOfSamples();
    auto totalY = yExtent_.getNumberOfSamples();


    z -= zExtent_.min;
    x -= xExtent_.min;
    y -= yExtent_.min;


    auto idxZ = std::round(z / zExtent_.step);
    auto idxX = std::round(x / xExtent_.step);
    auto idxY = std::round(y / yExtent_.step);

    return (idxZ * totalX * totalY) + (idxX * totalY) + idxY;
}
