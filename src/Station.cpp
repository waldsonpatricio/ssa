#include "Station.h"
#include "Crustal.h"

void Station::addLog(float time, float sWave, float psWave, float pWave)
{
    auto log   = Log{};
    log.otTime = time;
    log.sWave  = sWave;
    log.psWave = psWave;
    log.pWave  = pWave;

    auto index = logs_.size();

    if (pWave > max.pWave) {
        max.pWave        = pWave;
        maxIndexes.pWave = index;
    }

    if (sWave > max.sWave) {
        max.sWave        = sWave;
        maxIndexes.sWave = index;
    }

    if (psWave > max.psWave) {
        max.psWave        = psWave;
        maxIndexes.psWave  = index;
    }

    logs_.emplace_back(std::move(log));
}

void Station::addLog(float time, float sWave, float psWave, float pWave, float normalizedSWave, float normalizedPsWave, float normalizedPWave)
{
    auto log   = Log{};
    log.otTime = time;
    log.sWave  = sWave;
    log.psWave = psWave;
    log.pWave  = pWave;

    log.normalizedSWave  = normalizedSWave;
    log.normalizedPsWave = normalizedPsWave;
    log.normalizedPWave  = normalizedPWave;


    auto index = logs_.size();

    if (pWave > max.pWave) {
        max.pWave        = pWave;
        maxIndexes.pWave = index;
    }

    if (sWave > max.sWave) {
        max.sWave        = sWave;
        maxIndexes.sWave = index;
    }

    if (psWave > max.psWave) {
        max.psWave        = psWave;
        maxIndexes.psWave  = index;
    }

    logs_.push_back(std::move(log));
}


void Station::normalize()
{
    normalize(-1, 0);
}

void Station::normalize(size_t noSamplesAverage)
{
    normalize(noSamplesAverage, 0);
}

void Station::normalize(size_t noSamplesAverage, size_t offset)
{
    if (logs_.size() == 0) {
        return;
    }

    if (noSamplesAverage == size_t{0} || noSamplesAverage > logs_.size()) {
        for (auto& log : getLogs()) {

            log.normalizedSWave  /= log.sWave  / max.pWave;
            log.normalizedPsWave /= log.psWave / max.psWave;
            log.normalizedPWave  /= log.pWave  / max.pWave;
        }
    } else {
        //TODO: average sWave and PsWave
        auto size       = logs_.size();
        //auto max        = -std::numeric_limits<float>::infinity();
        auto iterations = std::ceil((static_cast<float>(size) - offset) / noSamplesAverage);

        for (auto i = 0; i < iterations; ++i) {
            auto begin = offset + static_cast<size_t>(i * noSamplesAverage);
            auto end   = offset + std::min(size - offset, begin + noSamplesAverage);

            auto maxP  = -std::numeric_limits<float>::infinity();
            auto maxS  = -std::numeric_limits<float>::infinity();
            auto maxPS = -std::numeric_limits<float>::infinity();

            for (auto i = begin; i < end; ++i) {
                auto& log = logs_[i];

                if (log.pWave > maxP) {
                    maxP = log.pWave;
                }

                if (log.sWave > maxS) {
                    maxS = log.sWave;
                }

                if (log.psWave > maxPS) {
                    maxPS = log.psWave;
                }
            }

            for (auto i = begin; i < end; ++i) {
                auto& log = logs_[i];
                log.normalizedSWave  = log.sWave  / maxS;
                log.normalizedPsWave = log.psWave / maxPS;
                log.normalizedPWave  = log.pWave  / maxP;
            }
            //to calculate the current ot, the normalized data only uses this part of logs
            break;
        }
    }
}



Station::Log operator+(float value, const Station::Log& log)
{
    auto out   = Station::Log{log};
    out.pWave  = value + log.pWave;
    out.sWave  = value + log.sWave;
    out.psWave = value + log.psWave;

    out.normalizedPWave  = value + log.normalizedPWave;
    out.normalizedSWave  = value + log.normalizedSWave;
    out.normalizedPsWave = value + log.normalizedPsWave;

    return out;
}

Station::Log operator-(float value, const Station::Log& log)
{
    auto out   = Station::Log{log};
    out.pWave  = value - log.pWave;
    out.sWave  = value - log.sWave;
    out.psWave = value - log.psWave;

    out.normalizedPWave  = value - log.normalizedPWave;
    out.normalizedSWave  = value - log.normalizedSWave;
    out.normalizedPsWave = value - log.normalizedPsWave;

    return out;
}

Station::Log operator*(float value, const Station::Log& log)
{
    auto out   = Station::Log{log};
    out.pWave  = value * log.pWave;
    out.sWave  = value * log.sWave;
    out.psWave = value * log.psWave;

    out.normalizedPWave  = value * log.normalizedPWave;
    out.normalizedSWave  = value * log.normalizedSWave;
    out.normalizedPsWave = value * log.normalizedPsWave;

    return out;
}

Station::Log operator/(float value, const Station::Log& log)
{
    auto out   = Station::Log{log};
    out.pWave  = value / log.pWave;
    out.sWave  = value / log.sWave;
    out.psWave = value / log.psWave;

    out.normalizedPWave  = value / log.normalizedPWave;
    out.normalizedSWave  = value / log.normalizedSWave;
    out.normalizedPsWave = value / log.normalizedPsWave;

    return out;
}

TravelTime Station::computeTravelTimeToPoint(Crustal& crustal, float x, float y, float z)
{
    auto currentLayerIndex = size_t{};
    auto currentLayer      = crustal.getLayer(z, currentLayerIndex);
    auto& layers           = crustal.getLayers();
    auto layerCount        = crustal.layersCount();

    return computeTravelTimeToPoint(
        currentLayer,
        currentLayerIndex,
        layers,
        layerCount,
        crustal,
        x, y, z
    );
}

TravelTime Station::computeTravelTimeToPoint(
    Layer& currentLayer, size_t currentLayerIndex, std::vector<Layer> layers,
    size_t layerCount, Crustal& crustal, float x, float y, float z
)
{
    /* auto xHash        = std::hash<float>()(x); */
    /* auto yHash        = std::hash<float>()(y); */
    /* auto zHash        = std::hash<float>()(z); */

    /* auto hash         = static_cast<long>((xHash << 3) ^ (yHash << 5) ^ (zHash << 7)); */
    auto hash         = getPositionHash(x, y, z);
    auto hashIterator = travelTimeCache_.find(hash);

    if (hashIterator != travelTimeCache_.end()) {
        return *hashIterator->second;
    }

    auto travelTime   = TravelTime{};
    travelTime.pWave  = std::numeric_limits<float>::infinity();
    travelTime.sWave  = std::numeric_limits<float>::infinity();

    if (layerCount ==  0) {
        throw std::invalid_argument{std::string{"There are not layers set in Crustal."}};
    }

    auto depths = std::vector<float>();
    for (size_t i = 0; i < layers.size() - 1; ++i) {
         depths.push_back(layers[i+1].depth);
    }
    depths.push_back(layers[layers.size() - 1].depth);

    auto thickness = std::vector<float>();
    thickness.push_back(depths[0]);
    for (size_t i = 1; i < depths.size(); ++i) {
        thickness.push_back(depths[i] - depths[i-1]);
    }


    /*if (currentLayerIndex == -1) {
        throw std::invalid_argument{std::string{"Crustal's speeds for depth not found."}};
    }*/


    if (std::abs(currentLayer.depth - z) < 0.01f) {
        return computeTravelTimeToPoint(
            crustal,
            x, y, z - 0.02f
        );
    }

    for (size_t i = 0; i < depths.size(); ++i) {
        if (z < depths[i]) {
            currentLayerIndex = i;
            break;
        }
    }

    if (this->z > currentLayer.depth) {
        throw std::invalid_argument{std::string{"Station depth is not valid to calculate travel time of waves to this point."}};
    }

    auto deltaX   = std::abs(this->x - x);
    auto deltaY   = std::abs(this->y - y);
    auto distance = std::sqrt(deltaX * deltaX + deltaY * deltaY);
    //to keep compatibility with fortran version
    size_t firstLayerIndex = 0;



    auto i1 = firstLayerIndex;
    auto i2 = currentLayerIndex;

    auto d1 = depths[firstLayerIndex] - this->z;
    auto d2 = z - depths[currentLayerIndex - 1];

    auto v1 = layers[firstLayerIndex].speed;
    auto v2 = layers[currentLayerIndex].speed;

    for (auto wave = 0; wave < 2; ++wave) {
        auto waveType = wave == 0 ? WaveType::pWave : WaveType::sWave;


        auto maxSpeed = -std::numeric_limits<float>::infinity();
        for (auto layerIndex = size_t{i1}; layerIndex <= i2; ++layerIndex) {
            auto currentSpeed = layers[layerIndex].getSpeedByWaveType(waveType);
            if (currentSpeed > maxSpeed) {
                maxSpeed = currentSpeed;
            }
        }

        auto ntype        = 0;
        auto dpar         = 0.2f;
        auto par          = -dpar;
        auto tmpDistance  = 0.0f;
//        auto time         = std::numeric_limits<float>::infinity();
        auto tmpTime      = 0.0f;
        auto maxInnerLoop = 20;
        auto maxLoop      = 20;

        while (true) {
            if (ntype == 0) {
                par = par + dpar;
            } else {
                par = par - dpar;
            }

            tmpDistance = 0;
            tmpTime     = 0;

            if (ntype > 0) {
                if (par > 0.000001f) {
                    for (auto i = size_t{i1 + 1}; i < i2 - 1; ++i) {
                        help2(par, maxSpeed, layers[i].getSpeedByWaveType(waveType), thickness[i], tmpDistance, tmpTime);
                    }
                    help2(par, maxSpeed, v1.getSpeedByWaveType(waveType), d1, tmpDistance, tmpTime);
                    help2(par, maxSpeed, v2.getSpeedByWaveType(waveType), d2, tmpDistance, tmpTime);
                } else {
                    maxInnerLoop--;
                    if (maxInnerLoop < 0) {
                        throw std::runtime_error{std::string{"Max inner loop count reached."}};
                    }
                    par  = par + dpar;
                    dpar = dpar/2.0f;
                    continue;
                }
            } else {
                if (par > .8f) {
                    ntype = 1;
                    par   = par - dpar;
                    par   = std::sqrt(1.0f - (par * par));
                    continue;
                }
                if (i1 + 1 < i2) {
                    for (auto i = size_t{i1 +1}; i <= i2 - 1; ++i) {
                        help1(par, maxSpeed, layers[i].getSpeedByWaveType(waveType), thickness[i], tmpDistance, tmpTime);
                    }
                }
                help1(par, maxSpeed, v1.getSpeedByWaveType(waveType), d1, tmpDistance, tmpTime);
                help1(par, maxSpeed, v2.getSpeedByWaveType(waveType), d2, tmpDistance, tmpTime);
            }
            if (currentLayerIndex == firstLayerIndex) {
                setTravelTime(
                    travelTime,
                    waveType,
                    std::sqrt(distance * distance + z * z) / layers[currentLayerIndex].speed.pWave
                );
                break;
            }
            auto distanceDiff = std::abs(tmpDistance - distance);
            if (distanceDiff < crustal.getDwa()) {
                if (travelTime.getSpeedByWaveType(waveType) > tmpTime) {
                    setTravelTime(travelTime, waveType, tmpTime);
                }
                break;
            }
            if (tmpDistance < distance) {
                continue;
            }

            if (ntype == 0) {
                par = par-dpar;
            } else {
                par = par+dpar;
            }

            dpar = dpar / 2;
            maxLoop--;

            if (maxLoop >= 0) {
                continue;
            }
        }

        auto currentLayerSpeed = layers[currentLayerIndex].getSpeedByWaveType(waveType);
        auto firstLayerSpeed   = layers[firstLayerIndex].getSpeedByWaveType(waveType);
        for (size_t i = currentLayerIndex; i < crustal.getLayers().size() - 1; ++i) {
            auto otherLayerSpeed = layers[i].getSpeedByWaveType(waveType);
            if (otherLayerSpeed < currentLayerSpeed || otherLayerSpeed < firstLayerSpeed) {
                continue;
            }
            tmpDistance = 0.0f;
            tmpTime     = 0.0f;
            par         = 1.0f / layers[i+1].getSpeedByWaveType(waveType);
            if (i != currentLayerIndex) {
                for (size_t j = currentLayerIndex + 1; j <= i; ++j) {
                    auto otherLayerSpeed = layers[j].getSpeedByWaveType(waveType);
                    auto nextLayerSpeed  = layers[i+1].getSpeedByWaveType(waveType);
                    if (otherLayerSpeed >= nextLayerSpeed) {
                        continue;
                    }
                    help3(par, otherLayerSpeed, thickness[j], tmpDistance, tmpTime);

                }
            }
            d1 = depths[currentLayerIndex] - this->z;
            help3(par, layers[currentLayerIndex].getSpeedByWaveType(waveType), d1, tmpDistance, tmpTime);
            if (i != firstLayerIndex) {
                for (size_t j = firstLayerIndex + 1; j <= i; ++j) {
                    auto thisLayerSpeed = layers[j].getSpeedByWaveType(waveType);
                    if (thisLayerSpeed >= layers[i+1].getSpeedByWaveType(waveType)) {
                        continue;
                    }
                    help3(par, thisLayerSpeed, thickness[j], tmpDistance, tmpTime);
                }
            }
            d1 = depths[firstLayerIndex] - this->z;
            help3(par, firstLayerSpeed, d1, tmpDistance, tmpTime);

            if (tmpDistance >= distance) {
                continue;
            }
            auto distanceDiff = std::abs(tmpDistance - distance);
            auto timeDiff     = distanceDiff/layers[i+1].speed.getSpeedByWaveType(waveType);
            auto timeSum      = tmpTime + timeDiff;
            if (travelTime.getSpeedByWaveType(waveType) > timeSum) {
                setTravelTime(travelTime, waveType, timeSum);
            }
        }
    }
    /* travelTimeCache_[hash] = make_unique<TravelTime>(travelTime.pWave, travelTime.sWave); */
//    cachePositionTravelTime(hash, travelTime.pWave, travelTime.sWave);
    return travelTime;
}

void Station::setTravelTime(TravelTime &travelTime, const WaveType &type, float time)
{
    switch (type) {
        case WaveType::pWave:
            travelTime.pWave = time;
            break;
        case WaveType::sWave:
            travelTime.sWave = time;
            break;
    }

}

long Station::getPositionHash(float x, float y, float z)
{
    auto xHash        = std::hash<float>()(x);
    auto yHash        = std::hash<float>()(y);
    auto zHash        = std::hash<float>()(z);

    return static_cast<long>((xHash << 3) ^ (yHash << 5) ^ (zHash << 7));
}

void Station::cachePositionTravelTime(float x, float y, float z, float pWave, float sWave)
{
    cachePositionTravelTime(getPositionHash(x, y, z), pWave, sWave);
}

void Station::cachePositionTravelTime(long hash, float pWave, float sWave)
{
    travelTimeCache_[hash] = make_unique<TravelTime>(pWave, sWave);
}

void Station::help1(float p, float vmax, float v, float d, float& td, float& tt)
{

    auto sin = p * v / vmax;
    auto cos = std::sqrt(std::abs(1.0f - sin * sin));
    auto tan = sin / cos;
    auto dd  = d * tan;
    td       = td + dd;
    auto dt  = d/v/cos;
    tt       = tt + dt;
}

//inverse of help1
void Station::help2(float p, float vmax, float v, float d, float& td, float& tt)
{
    auto sin = std::sqrt(1.0f - p * p) * v / vmax;
    auto cos = std::sqrt(std::abs(1.0f - sin * sin));
    auto tan = sin / cos;
    auto dd  = d * tan;
    td       = td + dd;
    auto dt  = d/v/cos;
    tt       = tt + dt;
}

void Station::help3(float p, float v, float d, float& td, float& tt)
{
    auto sin = p * v;
    auto cos = std::sqrt(std::abs(1.0f - sin * sin));
    auto tan = sin / cos;
    auto dd  = d * tan;
    td       = td + dd;
    auto dt  = d/v/cos;
    tt       = tt + dt;
}
