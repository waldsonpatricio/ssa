#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <mutex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>
#include <utility>

#include <boost/filesystem.hpp>
#include <thrust/host_vector.h>

#define VERBOSE 1

#include "Application.h"
#include "Crustal.h"
#include "ParallelSSA.h"
#include "SSA.h"
#include "StopWatch.h"
#include "cwd.h"
#include "functions.h"
#include "json.hpp"

#define VERSION "0.1"
//#define SSA_COMPUTE_S_AND_PS 1

using json = nlohmann::json;

struct aggregation_item {
    size_t count = 0;
    float sum = 0.0f;
};

struct aggregation
{
    std::vector<aggregation_item> sums;
};

std::unordered_map<size_t, aggregation> aggregations;

std::string getFileName(int number, int size, std::string extension);

std::string getFileName(std::string base_dir, int number, int size,
                        std::string extension);

void setupCrustal(Crustal& crustal, const json& config);

bool fileExists(std::string filename);

void processResult(const thrust::host_vector<float>& stack, const SSA& ssa,
                   const size_t firstOt, const json& config,
                   std::string output_dir);

void saveResultToFile(std::string output_dir, const SSA& ssa,
                      const SSAResult& result, size_t ot, float samplingRate, size_t numGood);

void addResultToInfoFile(const SSA& ssa, const SSAResult& result, size_t ot);

void saveAverages(const SSA& ssa, size_t groupSize, std::string saveDir, size_t max_ots, size_t min_count);

// project related functions
std::string getProjectFilename();
json createDefaultProjectJSON();
bool validateConfig(const json& config);
void run(const json& config, const std::string& stations_file,
         const std::string& station_file_template, std::string output_dir);

void runSequentially(const json& config, const std::string& stations_file,
         const std::string& station_file_template, std::string output_dir);

std::mutex save_info_file_mutex;
bool verbose = false;

#ifdef VERBOSE
template<typename Arg, typename... Args>
void log(Arg&& arg, Args&&... args) {
    if (!verbose) {
        return;
    }
    std::cout << std::forward<Arg>(arg);
    using expander = int[];
    (void)expander{0, (void(std::cout << std::forward<Args>(args)), 0)...};
    std::cout << std::endl;
}
#endif

int main(int argc, char** argv)
{
    namespace fs = boost::filesystem;

    auto projectFile = getProjectFilename();

    if (argc == 2) {
        std::string commandName = argv[1];
        if (commandName == "-v" || commandName == "--version") {
            std::cout << "Parallel SSA Version " << VERSION << "\n";
            return 1;
        } else if (commandName != "init") {
            std::cout << "Invalid call.\n";
            return -1;
        }

        if (fs::exists(projectFile)) {
            std::cout << "ssa.json exists. Aborting...\n";
            return -1;
        }

        auto defaultFileJSON = createDefaultProjectJSON();
        std::ofstream outFile{projectFile};

        outFile << std::setw(4) << defaultFileJSON;
        std::cout << "ssa.json created. Edit it before execute.\n";
        return 0;

    } else if (argc > 2) {
        std::cout << "Invalid arguments. Try again.\n";
        return -1;
    }

    if (!fs::exists(projectFile)) {
        std::cout << "ssa.json not found. Run '" << argv[0]
                  << " init' to create it.\n";
        return -1;
    }

    std::ifstream file{projectFile};

    json config;

    try {
        file >> config;
    } catch (nlohmann::detail::parse_error& e) {
        std::cout << e.what();
        return -1;
    }

    if (!validateConfig(config)) {
        return -1;
    }

    // cout << setw(4) << configJSON;

    try {


        auto working_dir = fs::current_path();

        fs::path input_dir = working_dir;

        if (config.count("inputDir") > 0 &&
            config["inputDir"].get<std::string>() != ".") {
            input_dir = input_dir / fs::path(config["inputDir"].get<std::string>());
        }

        const bool isSequential =
            config.count("sequential") > 0 && config["sequential"].get<bool>();

        verbose = config.count("verbose") > 0 ? config["verbose"].get<bool>() : true;

        // std::cout << fs::current_path().filename().string();

        // fs::path = inputPath;
        auto stationsFilename =
            fs::canonical(input_dir).string() / fs::path{"station.dat"};

        if (!fs::exists(stationsFilename)) {
            std::cout << "Stations file not found: '" << stationsFilename.string()
                      << "'. Aborting...\n";
            return -1;
        }

        std::string fileTemplate{"{name}raw.dat"};
        if (config.count("stationsFileTemplate") > 0) {
            fileTemplate = config["stationsFileTemplate"].get<std::string>();
        }

        const auto stationsFileTemplate = input_dir.string() + "/" + fileTemplate;

        const fs::path output_dir = {config.count("outputDir") == 0
                                         ? "."
                                         : config["outputDir"].get<std::string>()};

        if (!fs::exists(output_dir)) {
            std::cout << "Output dir doesn't exist. Creating " << output_dir << ".\n";
            fs::create_directory(output_dir);
        }

        const auto output_path = fs::canonical(output_dir);
        if (isSequential) {
            std::cout << "Running sequentially..." << std::endl;
            runSequentially(config, stationsFilename.string(), stationsFileTemplate,
                output_path.string());
        } else {
            std::cout << "Running in parallel..." << std::endl;
            /* cudaDeviceSynchronize(); */
            run(config, stationsFilename.string(), stationsFileTemplate,
                output_path.string());
            /*cudaDeviceReset();*/
        }
        //saving averages

        // std::cout << cwd << "\n";
    } catch (nlohmann::detail::parse_error& e) {
        std::cout << e.what();
        return -1;
    } catch (std::runtime_error& e) {
        std::cout << e.what();
    }


    return 0;
}

void saveAverages(const SSA& ssa, size_t groupSize, std::string saveDir, size_t maxOts, size_t minCount)
{
    size_t maxCount     = -std::numeric_limits<size_t>::infinity();
    float averageSum    = 0.0f;
    size_t averageCount = 0;

    for (auto kv : aggregations) {
        auto start            = kv.first * groupSize;
        auto end              = std::min(maxOts, (kv.first + 1) * groupSize - 1);
        auto average_filename = saveDir + "/average_" + std::to_string(start) + "_" + std::to_string(end) + ".dat";
        auto average_file     = std::ofstream(average_filename);

        average_file.precision(9);

#ifdef VERBOSE
        log("Saving average: ", start, ", ", end, ", ", average_filename);
#endif

        auto& aggr = kv.second;
        size_t currentIdx = 0;

        for (size_t iZ = 0; iZ < ssa.getZExtent().getNumberOfSamples(); iZ++) {
            auto z = ssa.getZExtent().min + (iZ * ssa.getZExtent().step);
            for (size_t iX = 0; iX < ssa.getXExtent().getNumberOfSamples(); iX++) {
                auto x = ssa.getXExtent().min + (iX * ssa.getXExtent().step);
                for (size_t iY = 0; iY < ssa.getYExtent().getNumberOfSamples(); iY++) {
                    auto y = ssa.getYExtent().min + (iY * ssa.getYExtent().step);

                    /* auto goodP = 0.0f; */

                    if (aggr.sums[currentIdx].count > maxCount) {
                        maxCount = aggr.sums[currentIdx].count;
                    }

                    averageSum += aggr.sums[currentIdx].count;
                    ++averageCount;

                    /* if (aggr.sums[currentIdx].count > 0 && aggr.sums[currentIdx].count >= minCount) { */
                    /*     goodP = aggr.sums[currentIdx].sum / aggr.sums[currentIdx].count; */
                    /* } */

                    average_file << x << "\t" << y << "\t" << z << "\t" << aggr.sums[currentIdx].sum
                        << '\t' << aggr.sums[currentIdx].count << '\n';

                    ++currentIdx;
                }
            }
        }

    }


#ifdef VERBOSE
    float averageAverage = averageSum / averageCount;
    log("Max Count: ", maxCount, "\tAverage Count: ", averageAverage);
#endif
}


bool validateConfig(const json& config)
{
    using namespace std;

    if (config.count("crustal") == 0) {
        cout << "'crustal' layers not set. Aborting...\n";
        return false;
    } else if (!config["crustal"].is_array()) {
        cout << "'crustal' must be an array. Aborting...\n";
        return false;
    } else if (config["crustal"].size() == 0) {
        cout << "'crustal' must not be empty. Aborting...\n";
        return false;
    }

    if (config.count("range") == 0) {
        cout << "'range' not set. Aborting...\n";
        return false;
    } else if (!config["range"].is_object()) {
        cout << "'range' must be an object. Aborting...\n";
        return false;
    } else if (config["range"].size() == 0) {
        cout << "'range' must not be empty. Aborting...\n";
        return false;
    }

    return true;
}

void run(const json& config, const std::string& stations_file,
         const std::string& station_file_template, std::string output_dir)
{
    namespace fs = boost::filesystem;

    try {
        auto& crustal = Crustal::getInstance();

        setupCrustal(crustal, config);

        std::string app_cache_file;
        std::string parallel_cache_file;

        app_cache_file      = "stations.cache";
        parallel_cache_file = "data.cache";

        Application app{stations_file, station_file_template, app_cache_file};
        app.prepare();

        auto& ssa = app.getSSA();

        auto range = config["range"];

        ssa.setXExtent(range["xMin"].get<double>(), range["xMax"].get<double>(),
                       range["xStep"].get<double>());
        ssa.setYExtent(range["yMin"].get<double>(), range["yMax"].get<double>(),
                       range["yStep"].get<double>());
        ssa.setZExtent(range["zMin"].get<double>(), range["zMax"].get<double>(),
                       range["zStep"].get<double>());

#ifdef VERBOSE
        log("# Points on grid: ", ssa.getNumberOfSamples(), "\n");
        log(
            "Grid dimensions: ",
            ssa.getXExtent().getNumberOfSamples(),
            "x",
            ssa.getYExtent().getNumberOfSamples(),
            "x",
            ssa.getZExtent().getNumberOfSamples(),
            "\n"
        );
#endif

        // user config
        const auto sampling_rate = config["samplingRate"].get<float>();
        const auto max_number_of_threads =
            config["maxFileSaveThreads"].get<size_t>();
        const auto max_number_of_parallel_ots = config["parallelOts"].get<size_t>();
        const auto security_margin =
            config["securityMargin"]
                .get<size_t>(); // the last <security_margin> ots will be ignored

        const auto max_ots =
            config["maxOts"].is_null()
                ? ssa.getStationAt(0).getLogs().size()
                : config["maxOts"]
                      .get<size_t>(); // std::numeric_limits<size_t>::infinity();
        const auto first_ot = config["firstOt"].get<size_t>();
        // end user config

        const size_t to_process =
            ssa.getStationAt(0).getLogs().size() - security_margin;

        size_t processed = first_ot;
        size_t otOffset  = ssa.getStationAt(0).getLogs()[0].otTime / sampling_rate;


        std::vector<std::thread> workers{};
        std::vector<std::unique_ptr<thrust::host_vector<float>>> results{};

        size_t i = 0;

        auto parallelSSA =
            ParallelSSA{ssa, 0.01f, sampling_rate, parallel_cache_file};

        while (processed < max_ots && processed < to_process) {
            auto processing =
                std::min(to_process - processed, max_number_of_parallel_ots);

#ifdef VERBOSE
            log("Computing OTs(", (processing), ", ", (processed + 1), "-", (processed + processing), ")\n");
#endif

            auto isLast = false;
            auto result = parallelSSA.compute(
                crustal,
                processing,
                processed,
                isLast,
                to_process,
                security_margin
            );

            results.push_back(std::move(result));

            std::thread t{processResult, *results[i], std::ref(ssa),
                          otOffset + processed, config, output_dir};
            workers.push_back(std::move(t));

            if (workers.size() == max_number_of_threads) {
                for (auto& thread : workers) {
                    thread.join();
                }
                // Here is safe to delete results (no longer used)
                workers.clear();
                results.clear();
                i = -1;
            }

            processed += processing;
            ++i;
        }

        for (auto& thread : workers) {
            thread.join();
        }
        // here is safe to delete results (no longer used)
        workers.clear();
        results.clear();

        auto willComputeAverage = config.count("averageEvery") > 0;
        int minCount = -1;
        if (config.count("averageMinCount") > 0) {
            minCount = config["averageMinCount"].get<int>();
        }

        if (willComputeAverage && minCount > 0) {
            auto averageEvery                 = config["averageEvery"].get<size_t>();
            const fs::path average_output_dir = output_dir + "/averages";

            if (!fs::exists(average_output_dir)) {
                std::cout << "Average output dir doesn't exist. Creating " << average_output_dir << ".\n";
                fs::create_directory(average_output_dir);
            }

#ifdef VERBOSE
            log("Saving averages(", averageEvery, ")");
#endif
            saveAverages(ssa, averageEvery, fs::canonical(average_output_dir).string(), max_ots - security_margin, minCount);
#ifdef VERBOSE
            log("Done.");
#endif
        }


        /*
    std::vector<std::thread> workers{};
    std::vector<std::unique_ptr<thrust::host_vector<float>>> results{};



    auto result      = parallelSSA.compute(crustal, 5000, 0);


    std::thread t{processResult, *result, std::ref(ssa)};


    t.join();*/
    } catch (std::invalid_argument& ex) {
        std::cout << "Error: " << ex.what() << std::endl;
    }
}

void runSequentially(const json& config, const std::string& stations_file,
        const std::string& station_file_template, std::string output_dir)
{
    namespace fs = boost::filesystem;

    try {
        auto& crustal = Crustal::getInstance();

        setupCrustal(crustal, config);

        std::string app_cache_file;
        std::string parallel_cache_file;

        app_cache_file      = "stations.cache";
        parallel_cache_file = "data.cache";

        Application app{stations_file, station_file_template, app_cache_file};
        app.prepare();

        bool hasTravelTimesCache = config.count("travelTimesCache") > 0 && !config["travelTimesCache"].is_null();
        if (hasTravelTimesCache) {
            app.loadTravelTimesFromFile(config["travelTimesCache"].get<std::string>());
        }

        auto& ssa = app.getSSA();

        auto range = config["range"];

        ssa.setXExtent(range["xMin"].get<float>(), range["xMax"].get<float>(),
                       range["xStep"].get<float>());
        ssa.setYExtent(range["yMin"].get<float>(), range["yMax"].get<float>(),
                       range["yStep"].get<float>());
        ssa.setZExtent(range["zMin"].get<float>(), range["zMax"].get<float>(),
                       range["zStep"].get<float>());

#ifdef VERBOSE
        log("# Points on grid: ", ssa.getNumberOfSamples(), "\n");
#endif

        // user config
        const auto sampling_rate = config["samplingRate"].get<float>();
        const auto security_margin =
            config["securityMargin"]
                .get<size_t>(); // the last <security_margin> ots will be ignored

        const auto max_ots =
            config["maxOts"].is_null()
                ? ssa.getStationAt(0).getLogs().size()
                : config["maxOts"]
                      .get<size_t>(); // std::numeric_limits<size_t>::infinity();

        auto first_ot = config["firstOt"].get<size_t>();
        // end user config

        size_t otOffset = ssa.getStationAt(0).getLogs()[0].otTime / sampling_rate;

        const size_t to_process =
            ssa.getStationAt(0).getLogs().size() - security_margin;

        size_t processed = first_ot;


        while (processed < max_ots && processed < to_process) {

            auto result = ssa.run(crustal, sampling_rate, (first_ot * sampling_rate) + (processed * sampling_rate));
#ifdef VERBOSE
            log("OT #", (processed + 1), ": ", (result.getExecutionTime()),  ", MaxP: ", result.getMaximumPWaveBrightnessInfo().goodP, ")\n");
#endif
            auto max_info    = result.getMaximumPWaveBrightnessInfo();
            ++processed;

            if (config.count("fileSaveFilter") > 0) {
                auto filters = config["fileSaveFilter"];

                if (filters.count("goodP") > 0 && filters["goodP"].is_number()) {
                    auto good_p = filters["goodP"].get<float>();

                    if (max_info.goodP < good_p) {
                        continue;
                    }
                }

                if (filters.count("ratGood") > 0 && filters["ratGood"].is_number()) {
                    auto rat_good = filters["ratGood"].get<float>();

                    if (result.getRatGoodP() < rat_good) {
                        continue;
                    }
                }

                if (filters.count("ratNum") > 0 && filters["ratNum"].is_number()) {
                    auto rat_num = filters["ratNum"].get<float>();

                    if (result.getRatNumP() < rat_num) {
                        continue;
                    }
                }
            }

            saveResultToFile(output_dir, ssa, result, first_ot + processed + otOffset,
                    sampling_rate, result.getNumP());
        }

        auto willComputeAverage = config.count("averageEvery") > 0;
        int minCount = -1;
        if (config.count("averageMinCount") > 0) {
            minCount = config["averageMinCount"].get<int>();
        }

        if (willComputeAverage) {
            auto averageEvery                 = config["averageEvery"].get<size_t>();
            const fs::path average_output_dir = output_dir + "/averages";

            if (!fs::exists(average_output_dir)) {
                std::cout << "Average output dir doesn't exist. Creating " << average_output_dir << ".\n";
                fs::create_directory(average_output_dir);
            }

#ifdef VERBOSE
            log("Saving averages(", averageEvery, ")");
#endif
            saveAverages(ssa, averageEvery, fs::canonical(average_output_dir).string(), max_ots - security_margin, minCount);
#ifdef VERBOSE
            log("Done.");
#endif
        }


        /*
    std::vector<std::thread> workers{};
    std::vector<std::unique_ptr<thrust::host_vector<float>>> results{};



    auto result      = parallelSSA.compute(crustal, 5000, 0);


    std::thread t{processResult, *result, std::ref(ssa)};


    t.join();*/
    } catch (std::invalid_argument& ex) {
        std::cout << "Error: " << ex.what() << std::endl;
    }


}

std::string getProjectFilename()
{
    return get_current_working_dir() + "/ssa.json";
}

nlohmann::json createDefaultProjectJSON()
{
    json projectFile;
    projectFile["outputDir"] = "result";
    projectFile["range"]     = {{"xMin", 0}, {"xMax", 0}, {"xStep", 0}, {"yMin", 0}, {"yMax", 0}, {"yStep", 0}, {"zMin", 0}, {"zMax", 0}, {"zStep", 0}};

    projectFile["crustal"] = {
        json::object({{"depth", 0}, {"pw", 1.3}, {"sw", 0.75}}),
        json::object({{"depth", 0.075}, {"pw", 2.3}, {"sw", 1.3}}),
        json::object({{"depth", 0.375}, {"pw", 4.20}, {"sw", 2.43}})};

    projectFile["fileSaveFilter"] = {
        {"goodP", nullptr}, {"ratGood", nullptr}, {"ratNum", nullptr}};

    projectFile["samplingRate"]              = 0.005;
    projectFile["brightnessCutoffMultipler"] = 0.8;
    projectFile["parallelOts"]               = 1000;
    projectFile["maxFileSaveThreads"]        = 4;
    projectFile["securityMargin"]            = 100;
    projectFile["inputDir"]                  = ".";
    /* projectFile["firstOt"]                   = 0; */
    projectFile["maxOts"]                    = nullptr;
    projectFile["stationFileTemplate"]       = "{name}raw.dat";
    projectFile["averageEvery"]              = 0;
    projectFile["averageMinBrightness"]      = -1000000000.00;
    projectFile["averageMinCount"]           = -1000000000.00;
    projectFile["sequential"]                = false;
    projectFile["verbose"]                   = true;
    projectFile["travelTimesCache"]          = nullptr;

    return projectFile;
}

bool fileExists(std::string filename)
{
    std::ifstream f{filename};
    return f.good();
}

void processResult(const thrust::host_vector<float>& stack, const SSA& ssa,
                   const size_t firstOt, const json& config,
                   std::string output_dir)
{

    size_t resultCount      = stack.size();
    size_t numberOfSamples  = ssa.getNumberOfSamples();
    size_t numberOfStations = ssa.getNumberOfStations();
    size_t numberOfOts      = resultCount / numberOfSamples;



    auto start          = stack.begin();
    auto end            = start + numberOfOts;
    auto offset         = size_t{0};
    auto min_brightness = std::numeric_limits<float>::infinity();
    auto max_brightness = -std::numeric_limits<float>::infinity();
    auto averageGoodP   = -std::numeric_limits<float>::infinity();


    if (config.count("averageMinBrightness") > 0) {
        averageGoodP = config["averageMinBrightness"].get<float>();
    }

    StopWatch watch{};
    watch.start();
    size_t saved_files{0};

    auto willComputeAverage    = config.count("averageEvery") > 0;
    size_t computeAverageEvery = 0;
    size_t currentAverageIndex = 0;

    if (willComputeAverage) {
        computeAverageEvery = config["averageEvery"].get<size_t>();
    }

    for (size_t i = 0; i < numberOfOts; ++i) {
        SSAResult result{};

        float brPmax        = -std::numeric_limits<float>::infinity();
        size_t brPmaxIndex  = 0;
        size_t currentIndex = size_t{0};

        aggregation* currentAgg = nullptr;

        offset = i * numberOfSamples;
        /* std::cout << "OFFSET: " << offset << " ------ SIZE: " << stack.size() << " ---- OTS: " */
        /*     << i << "/" << numberOfOts << " ------ SAMPLES " << numberOfSamples << std::endl; */

        /* offset = 0; */

        if (willComputeAverage) {
            currentAverageIndex = (firstOt + i) / computeAverageEvery;
            if (aggregations.find(currentAverageIndex) == aggregations.end()) {
                aggregation agg;
                aggregations[currentAverageIndex] = agg;
            }
            currentAgg = &aggregations[currentAverageIndex];
        }

        for (size_t iZ = 0; iZ < ssa.getZExtent().getNumberOfSamples(); iZ++) {
            auto z = ssa.getZExtent().min + (iZ * ssa.getZExtent().step);
            for (size_t iX = 0; iX < ssa.getXExtent().getNumberOfSamples(); iX++) {
                auto x = ssa.getXExtent().min + (iX * ssa.getXExtent().step);
                for (size_t iY = 0; iY < ssa.getYExtent().getNumberOfSamples(); iY++) {
                    auto y = ssa.getYExtent().min + (iY * ssa.getYExtent().step);
                    float goodP = 0;
                    try {
                        goodP = stack[currentIndex + offset] / numberOfStations;
                    } catch (std::out_of_range&) {
                        std::cout << "OUT OF RANGE: " << (currentIndex + offset) << "/" << resultCount << std::endl;
                        continue;
                    }

                    if (goodP > brPmax) {
                        brPmax      = goodP;
                        brPmaxIndex = currentIndex;
                    }

                    result.addInfo(x, y, z, numberOfStations, 0,
                                   stack[currentIndex + offset], 0, goodP, 0, 0);

                    if (willComputeAverage) {

                        if (currentAgg->sums.size() <= currentIndex) { //if it is here for the first time
                            aggregation_item item;
                            currentAgg->sums.push_back(item);
                        }

                        if (goodP >= averageGoodP) {
                            currentAgg->sums[currentIndex].sum += goodP;
                            ++currentAgg->sums[currentIndex].count;
                        }
                    }

                    ++currentIndex;
                }
            }
        }

        auto cutoffMultiplier  = config["brightnessCutoffMultipler"].get<float>();
        auto brightnessCutoffP = cutoffMultiplier * brPmax;

        auto numP = 0;

        for (auto& infoPtr : result.getInfos()) {
            auto& info = *infoPtr;

            if (info.goodP > brightnessCutoffP) {
                ++numP;
            }
        }

        result.ratNumP_                         = static_cast<float>(numP) / numberOfSamples;
        result.ratGoodP_                        = brPmax / result.ratNumP_;
        result.maximumPWaveBrightnessInfoIndex_ = brPmaxIndex;

        ///
        start = end;
        end   = start + numberOfOts;

        const auto& max_info = result.getMaximumPWaveBrightnessInfo();

        if (max_info.goodP > max_brightness) {
            max_brightness = max_info.goodP;
        }

        if (max_info.goodP < min_brightness) {
            min_brightness = max_info.goodP;
        }

        if (config.count("fileSaveFilter") > 0) {
            auto filters = config["fileSaveFilter"];

            if (filters.count("goodP") > 0 && filters["goodP"].is_number()) {
                auto good_p = filters["goodP"].get<float>();

                if (max_info.goodP < good_p) {
                    continue;
                }
            }

            if (filters.count("ratGood") > 0 && filters["ratGood"].is_number()) {
                auto rat_good = filters["ratGood"].get<float>();

                if (result.getRatGoodP() < rat_good) {
                    continue;
                }
            }

            if (filters.count("ratNum") > 0 && filters["ratNum"].is_number()) {
                auto rat_num = filters["ratNum"].get<float>();

                if (result.getRatNumP() < rat_num) {
                    continue;
                }
            }
        }

        saveResultToFile(output_dir, ssa, result, firstOt + i + 1,
                         config["samplingRate"].get<float>(), numP);
        ++saved_files;
    }
    watch.stop();
#ifdef VERBOSE
    auto b = firstOt + 1;
    auto e = firstOt + numberOfOts;
    if (verbose) {
        log("########################################");
        log("# processed OTs (", b, ", ", e, "): ", numberOfOts, "\t Time to process: ", watch.getTimeElasped() / 1000.0f, "\t Saved files:", saved_files, "\n");
        log("Min brightness: ", min_brightness, "\tMax brightness: ", max_brightness, "\n");
        log("########################################");
    }
#endif
}

void addResultToInfoFile(const SSA& ssa, const SSAResult& result, size_t ot) {}

void saveResultToFile(std::string output_dir, const SSA& ssa,
                      const SSAResult& result, size_t ot, float samplingRate, size_t numGood)
{
    const auto& max_brightness_info = result.getMaximumPWaveBrightnessInfo();


    auto file    = std::ofstream{getFileName(output_dir, ot, 5, "dat")};
    auto xy_file = std::ofstream{getFileName(output_dir, ot, 5, "xy")};
    auto xz_file = std::ofstream{getFileName(output_dir, ot, 5, "xz")};
    auto yz_file = std::ofstream{getFileName(output_dir, ot, 5, "yz")};

    file.precision(9);

    auto i                    = size_t{1};
    auto iz                   = size_t{1};
    auto max_brightness_index = -1;


    const auto x_max = max_brightness_info.x;
    const auto y_max = max_brightness_info.y;
    const auto z_max = max_brightness_info.z;

    //xy
    for (size_t iX = 0; iX < ssa.getXExtent().getNumberOfSamples(); iX++) {
        auto x = ssa.getXExtent().min + (iX * ssa.getXExtent().step);
        size_t y_index{0};

        for (size_t iY = 0; iY < ssa.getYExtent().getNumberOfSamples(); iY++) {
            auto y = ssa.getYExtent().min + (iY * ssa.getYExtent().step);
            auto& info = result.getInfoAt(ssa.getIndexOfPosition(x, y, z_max));
            //xyz(dat file)
            for (size_t iZ = 0; iZ < ssa.getZExtent().getNumberOfSamples(); iZ++) {
                auto z = ssa.getZExtent().min + (iZ * ssa.getZExtent().step);
                auto& zInfo = result.getInfoAt(ssa.getIndexOfPosition(x, y, z));
                file << zInfo.x << "\t" << zInfo.y << "\t" << zInfo.z << "\t" << zInfo.goodP << "\t" << iz <<  '\n';

                ++iz;
            }

            xy_file << info.x << "\t" << info.y << "\t" << info.z << "\t" << info.goodP << '\n';

            if (&info == &max_brightness_info) {
                max_brightness_index = i;
            }
            ++y_index;
            ++i;
        }
    }

    //xz
    for (size_t iX = 0; iX < ssa.getXExtent().getNumberOfSamples(); iX++) {
        auto x = ssa.getXExtent().min + (iX * ssa.getXExtent().step);
        for (size_t iZ = 0; iZ < ssa.getZExtent().getNumberOfSamples(); iZ++) {
            auto z = ssa.getZExtent().min + (iZ * ssa.getZExtent().step);
            auto& info = result.getInfoAt(ssa.getIndexOfPosition(x, y_max, z));
            xz_file << info.x << "\t" << info.y << "\t" << info.z << "\t" << info.goodP << '\n';
        }
    }

    //yz
    for (size_t iY = 0; iY < ssa.getYExtent().getNumberOfSamples(); iY++) {
        auto y = ssa.getYExtent().min + (iY * ssa.getYExtent().step);
        for (size_t iZ = 0; iZ < ssa.getZExtent().getNumberOfSamples(); iZ++) {
            auto z = ssa.getZExtent().min + (iZ * ssa.getZExtent().step);
            auto& info = result.getInfoAt(ssa.getIndexOfPosition(x_max, y, z));
            yz_file << info.x << "\t" << info.y << "\t" << info.z << "\t" << info.goodP << '\n';
        }
    }

    auto ot_time = ot * samplingRate;
    // add row on info file synchronously
    std::lock_guard<std::mutex> lock{save_info_file_mutex};

    std::ofstream info_file;
    info_file.open(output_dir + "/output.info", std::ios::out | std::ios::app);

    info_file << std::setprecision(9) << std::fixed << std::showpoint;

    info_file << ot_time << "\t";
    info_file << max_brightness_info.x << "\t";
    info_file << max_brightness_info.y << "\t";
    info_file << max_brightness_info.z << "\t";
    info_file << max_brightness_info.goodP << "\t";
    info_file << max_brightness_index << "\t";
    info_file << ot << "\t";
    info_file << numGood << "\t";
    info_file << result.getRatGoodP() << "\n";
    // std::cout << "Tempo(" << j << ") = " << result.getExecutionTime() << '\t'
    // << result.getMaximumPWaveBrightnessInfo().goodP << '\n';
}

void setupCrustal(Crustal& crustal, const json& config)
{
    auto& crustalConfig = config["crustal"];

    for (auto& layer : crustalConfig) {
        crustal.addLayer(layer["depth"].get<float>(), layer["pw"].get<float>(),
                         layer["sw"].get<float>());
    }
}

std::string getFileName(std::string base_dir, int number, int size,
                        std::string extension)
{
    auto stream = std::stringstream();
    stream << base_dir << "/";

    auto power = long{static_cast<long>(std::pow(10, size - 1))};

    while (power > number) {
        stream << "0";
        power /= 10;
    }

    if (number > 0) {
        stream << number;
    }

    stream << "." << extension;
    return stream.str();
}

std::string getFileName(int number, int size, std::string extension)
{
    return getFileName(std::string{"result/"}, number, size, extension);
}
