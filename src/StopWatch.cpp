#include <ratio>

#include "StopWatch.h"



StopWatch::StopWatch() :
    running_{true},
    start_{std::chrono::high_resolution_clock::now()},
    end_{std::chrono::high_resolution_clock::now()}
{}

void StopWatch::start()
{
    start_   = std::chrono::high_resolution_clock::now();
    running_ = true;
}


void StopWatch::stop()
{
    end_    = std::chrono::high_resolution_clock::now();
    running_ = false;
}


double StopWatch::getTimeElasped()
{
    if (running_) {
        end_ = std::chrono::high_resolution_clock::now();
    }
    auto duration = std::chrono::duration<double, std::milli>{end_ - start_};
    return duration.count();
}
