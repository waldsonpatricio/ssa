#include "Application.h"

#include <stdexcept>

std::string Application::filenamePlaceholder = std::string{"{name}"};

Application::Application(
    std::string stationsFile,
    std::string stationFileTemplate,
    std::string cacheFile)
    : ssa_()
    , dataPower_(0.0f)
    , stationFileTemplate_(stationFileTemplate)
    , cacheFile_(cacheFile)
    , isFirstRun_(true)
{
    Opener<std::ifstream> stationsFileStream{stationsFile};

    auto line        = std::string{};
    auto currentLine = std::size_t(0);


    while (std::getline(*stationsFileStream, line)) {
        currentLine++;
        if (currentLine <= 2 || line.length() == 0) { //ignoring header (2 lines)
            continue;
        }

        auto stream = std::istringstream{""};
        stream.str(line);

        auto x       = 0.0f;
        auto y       = 0.0f;
        auto z       = 0.0f;
        auto garbage = std::string{""}; //to skip columns that don't matter
        auto name    = std::string{""};

        stream >> x;
        stream >> y;
        stream >> z;
        stream >> garbage;
        stream >> garbage;
        stream >> name;

        ssa_.addStation(name, x, y, z);
    }
}

std::string Application::getStationDataFilename(const Station& station)
{
    auto foundPos = stationFileTemplate_.find(filenamePlaceholder);

    if (foundPos == std::string::npos) {
        return stationFileTemplate_;
    }

    auto result = stationFileTemplate_;
    auto& name  = station.name;

    result.replace(foundPos, filenamePlaceholder.length(), name);

    return result;
}

SSAResult Application::run(Crustal& crustal, float originTime, float samplingInterval)
{
    return ssa_.run(crustal, samplingInterval, originTime);
}


void Application::prepare()
{
    //checkCache();
    if (isFirstRun_) {
        populateStations();
        isFirstRun_ = false;
        //writeToCache();
    }
}

void Application::populateStations()
{
    for (auto& stationPtr : ssa_.getStations()) {
        auto& station = *stationPtr;
        auto filename = getStationDataFilename(station);

        Opener<std::ifstream> stationFile{filename};

        auto stream = std::istringstream{""};
        auto line   = std::string{};

        auto time   = 0.0f;
        auto pWave  = 0.0f;
        auto sWave  = 0.0f;
        auto psWave = 0.0f;

        stationsMap_[station.getName()] = &station;

        while (std::getline(*stationFile, line)) {
            stream.clear();
            stream.str(line);

            stream >> time;

            stream >> sWave;
            stream >> psWave;
            stream >> pWave;

            station.addLog(time, sWave, psWave, pWave);
        }
    }
}

void Application::loadTravelTimesFromFile(const std::string& filename)
{
    Opener<std::ifstream> travelTimesFile{filename};
    auto stream = std::istringstream{""};
    auto line   = std::string{};

    auto x     = 0.0f;
    auto y     = 0.0f;
    auto z     = 0.0f;
    auto pWave = 0.0f;
    auto sWave = 0.0f;
    std::string stationName;

    int ignoreInt = 0;


    while (std::getline(*travelTimesFile, line)) {
        stream.clear();
        stream.str(line);

        stream >> ignoreInt; //contador de pontos do grid
        stream >> ignoreInt; //contador de estações

        stream >> pWave; //tempo de percurso da onda p
        stream >> sWave; //tempo de percurso da onda s

        //position
        stream >> x;
        stream >> y;
        stream >> z;


        stream >> ignoreInt; //contador de estações
        stream >> stationName; //nome da estação

        Station* station = getStationByName(stationName);
        if (!station) {
            throw std::invalid_argument{
                std::string{"Station with name '" + filename + "' could not be found."}
            };
        }
        station->cachePositionTravelTime(x, y, z, pWave, sWave);
    }

}

Station* Application::getStationByName(const std::string& name)
{
    return stationsMap_[name];
}

SSA& Application::getSSA()
{
    return ssa_;
}

void Application::checkCache()
{
    if (!isFirstRun_) {
        return;
    }
    std::ifstream cache_file{cacheFile_, std::ios::binary};
    if (!cache_file.good()) {
        return;
    }
    readFromCache(cache_file);
}

void Application::readFromCache(std::ifstream& file)
{
    if (!isFirstRun_) {
        return;
    }
    using logs_type = std::vector<std::unique_ptr<Station::Log>>::value_type;

    //data power
    file.read(reinterpret_cast<char*>(&dataPower_), sizeof(double));

    //number of stations
    size_t numberOfStations = 0;
    file.read(reinterpret_cast<char*>(&numberOfStations), sizeof_size_field);

    for (size_t i = 0; i < numberOfStations; i++) {
        //get size of station name
        size_t nameSize = 0;
        file.read(reinterpret_cast<char*>(&nameSize), sizeof_size_field);

        //get station name
        std::string name;
        name.resize(nameSize);
        file.read(reinterpret_cast<char*>(&name[0]), nameSize);

        //get station coords
        float x = 0;
        float y = 0;
        float z = 0;

        file.read(reinterpret_cast<char*>(&x), sizeof(float));
        file.read(reinterpret_cast<char*>(&y), sizeof(float));
        file.read(reinterpret_cast<char*>(&z), sizeof(float));


        //get number of logs
        size_t numberOfLogs = 0;
        file.read(reinterpret_cast<char*>(&numberOfLogs), sizeof_size_field);

        ssa_.addStation(name, x, y, z);

        Station& s = ssa_.getStationAt(i);

        s.logs_.resize(numberOfLogs);

        file.read(reinterpret_cast<char*>(&s.logs_[0]), sizeof(logs_type) * numberOfLogs);
    }
    isFirstRun_ = false;
}


void Application::writeToCache()
{
    if (isFirstRun_) {
        return;
    }

    std::ofstream cache_file{cacheFile_, std::ios::binary};
    auto numberOfStations = getSSA().getStations().size();

    //data_power
    cache_file.write(reinterpret_cast<const char*>(&dataPower_), sizeof(double));
    //number of stations
    cache_file.write(reinterpret_cast<const char*>(&numberOfStations), sizeof_size_field);


    using logs_type = std::vector<std::unique_ptr<Station::Log>>::value_type;
    //station logs
    for (auto& sp : ssa_.getStations()) {
        Station& s        = (*sp);
        auto numberOfLogs = s.getLogs().size();
        auto name         = s.getName();
        auto nameSize     = name.size();

        //write size of station name
        cache_file.write(reinterpret_cast<const char*>(&nameSize), sizeof_size_field);

        //write station name
        cache_file.write(name.c_str(), nameSize);

        //write station coord
        cache_file.write(reinterpret_cast<const char*>(&s.x), sizeof(float));
        cache_file.write(reinterpret_cast<const char*>(&s.y), sizeof(float));
        cache_file.write(reinterpret_cast<const char*>(&s.z), sizeof(float));

        //write number of logs
        cache_file.write(reinterpret_cast<const char*>(&numberOfLogs), sizeof_size_field);

        //write logs
        auto& logs = s.getLogs();

        cache_file.write(reinterpret_cast<const char*>(&logs[0]), sizeof(logs_type) * numberOfLogs);
    }
}

/*
void ParallelSSA::check_cache()
{
    if (travelTimesComputed_) {
        return;
    }
    std::ifstream cache_file{cacheFile_, std::ios::binary};
    if (!cache_file.good()) {
        return;
    }
    read_from_cache(cache_file);
}

void ParallelSSA::read_from_cache(std::ifstream& file)
{

    if (travelTimesComputed_) {
        return;
    }
    auto start =  std::chrono::system_clock::now();
    std::cout << "Reading from cache...\n";
    size_t size = 0;
    file.read(reinterpret_cast<char*>(&size), sizeof_cache_header);

    values_.clear();
    values_.resize(size);
    file.read(reinterpret_cast<char *>(&values_[0]), size * sizeof(float));

    size_t offsetSize = 0;
    file.read(reinterpret_cast<char*>(&offsetSize), sizeof_cache_header);
    offsets_.clear();
    offsets_.resize(offsetSize);
    file.read(reinterpret_cast<char *>(&offsets_[0]), size * sizeof(size_t));

    travelTimesComputed_ = true;

    auto end = std::chrono::system_clock::now();
    auto timeToRead = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() / 1000.0f;
    std::cout << "Lido do cache em " << timeToRead << "s\n";
}

void ParallelSSA::write_to_cache()
{
    if (!travelTimesComputed_) {
        return;
    }
    std::ofstream cache_file{cacheFile_, std::ios::binary};
    auto size       = values_.size();
    auto offsetSize = offsets_.size();

    cache_file.write(reinterpret_cast<const char*>(&size), sizeof_cache_header);
    cache_file.write(reinterpret_cast<const char*>(&values_[0]), sizeof(float) * size);

    cache_file.write(reinterpret_cast<const char*>(&offsetSize), sizeof_cache_header);
    cache_file.write(reinterpret_cast<const char*>(&offsets_[0]), sizeof(size_t) * size);
}
 */
