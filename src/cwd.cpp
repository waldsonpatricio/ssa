#include "cwd.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <direct.h>
#define get_current_dir _getcwd
#else
#include <unistd.h>
#define get_current_dir getcwd
#endif




std::string get_current_working_dir()
{
    char buff[FILENAME_MAX];

    auto foo = get_current_dir(buff, FILENAME_MAX );

    std::string current_working_dir(buff);

    return current_working_dir;
}



