#include "compute_ots.h"

#include <iostream>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/transform.h>
#include <thrust/reduce.h>
#include <thrust/system_error.h>

using namespace thrust;

template<typename T>
struct transform_linear_index_to_row_index : unary_function<T, T> {

    transform_linear_index_to_row_index(T numberOfColumns) :
        numberOfColumns_{numberOfColumns}
    {}

    __host__ __device__ T operator()(T i) {
        return i / numberOfColumns_;
    }

    private:
        T numberOfColumns_;
};

template<typename T>
struct transform_linear_index_to_col_index : unary_function<T, T> {

    transform_linear_index_to_col_index(T numberOfColumns) :
        numberOfColumns_{numberOfColumns}
    {}

    __host__ __device__ T operator()(T i) {
        return i % numberOfColumns_;
    }

    private:
        T numberOfColumns_;
};


//Input must be a Tuple of <index, sequential>
template<typename Input, typename Output>
struct transform_index_plus_ot : unary_function<Input, Output> {

    transform_index_plus_ot(size_t numberOfStations, size_t numberOfLogsPerStation, size_t numberOfItemsPerOt, size_t otOffset) :
        numberOfStations_{numberOfStations},
        numberOfLogsPerStation_{numberOfLogsPerStation},
        numberOfItemsPerOt_{numberOfItemsPerOt} ,
        otOffset_{otOffset}
    {}

    __host__ __device__ Output operator()(const Input& i) {
        auto index      = size_t{get<0>(i)};
        auto sequential = get<1>(i);

        auto station = static_cast<size_t>(sequential % numberOfStations_);
        auto ot      = static_cast<size_t>(sequential / numberOfItemsPerOt_);


        return (station * numberOfLogsPerStation_) + index + (ot + otOffset_);
    }

    private:
        size_t numberOfStations_;
        size_t numberOfLogsPerStation_;
        size_t numberOfItemsPerOt_;
        size_t otOffset_;//
};

void compute_ots(
    size_t numberOfStations,
    size_t numberOfLogsPerStation,
    size_t numberOfSamplesPerOt,
    size_t numberOfOts,
    size_t firstOt, //offset
    const host_vector<float>& logs,
    const host_vector<size_t>& distanceOffsets,
    host_vector<float>& result
) {
    try {
        if (numberOfOts == 0 || numberOfLogsPerStation == 0) {
            std::cout << "Empty logs. Aborting...\n";
            return;
        }
        //values[station0:log0,log1..., station1:log0,log1...]
        //offsets[position0:station0,station1..., position1:station0,station1...]
        using thrust::counting_iterator;

        //const auto outputSize = size_t{numberOfSamplesPerOt * numberOfOts};
        const auto sumSize    = size_t{numberOfStations * numberOfSamplesPerOt * numberOfOts};

        device_vector<float> deviceLogs     = logs;
        device_vector<size_t> deviceDistances = distanceOffsets;

        //transform indices of distanceOffsets in array values
        //auto deviceSumValues  = device_vector<float>{};
        auto deviceSumIndices = device_vector<size_t>{};

        //std::cout << sumSize << '\t' << numberOfOts;

        auto keysIteratorBegin     = make_transform_iterator(
            counting_iterator<size_t>{0},
            transform_linear_index_to_col_index<size_t>{static_cast<size_t>(numberOfStations * numberOfSamplesPerOt)}
        );

        /*auto keysIteratorEnd     = make_transform_iterator(
          counting_iterator<int>{0},
          transform_linear_index_to_col_index<int>{numberOfStations * numberOfSamplesPerOt}
          ) + sumSize;*/


        //allIndices contains indices of all OTs (not offset'd yet, though)
        using SumValuesIterator  = decltype(deviceDistances.begin());
        using SumIndicesIterator = decltype(keysIteratorBegin);
        auto indicesRepetead     = permutation_iterator<SumValuesIterator, SumIndicesIterator>{deviceDistances.begin(), keysIteratorBegin};


        auto allIndicesCounting = counting_iterator<size_t>{0};

        using AllIndicesTuple    = tuple<decltype(indicesRepetead), decltype(allIndicesCounting)>;
        using AllIndicesIterator = zip_iterator<AllIndicesTuple>;


        auto allIndicesTupleIterator = AllIndicesIterator{make_tuple(indicesRepetead, allIndicesCounting)};

        auto allIndices = make_transform_iterator(
            allIndicesTupleIterator,
            //TODO: last parameter should be replaced by ot offset and be set from a function parameter
            transform_index_plus_ot<thrust::tuple<int, size_t>, size_t>{numberOfStations, numberOfLogsPerStation, numberOfStations * numberOfSamplesPerOt, firstOt}
        );


        auto allValuesIterator = permutation_iterator<decltype(deviceLogs.begin()), decltype(allIndices)>{deviceLogs.begin(), allIndices};

        auto allValuesIteratorBegin     = make_transform_iterator(
            counting_iterator<size_t>{0},
            transform_linear_index_to_row_index<size_t>{numberOfStations}
        );

        auto allValuesIteratorEnd     = make_transform_iterator(
            counting_iterator<size_t>{0},
            transform_linear_index_to_row_index<size_t>{numberOfStations}
        ) + sumSize;


        auto sumAllValuesPerPosition = device_vector<float>{numberOfOts * numberOfSamplesPerOt};

        reduce_by_key(
            allValuesIteratorBegin,
            allValuesIteratorEnd,
            allValuesIterator,
            make_discard_iterator(),
            sumAllValuesPerPosition.begin(),
            equal_to<size_t>{},
            plus<float>()
        );



        thrust::copy(sumAllValuesPerPosition.begin(), sumAllValuesPerPosition.begin() + (numberOfOts * numberOfSamplesPerOt), result.begin());
    } catch (thrust::system_error& e) {
        std::cout << "GPU Code Exception:" << e.what() << "\n";
        throw;
    }

    //sum by position

   //auto allIndices
    //deviceSumIndices.reserve(sumSize);
    //deviceSumValues.reserve(sumSize);


//    for (auto i = keysIteratorBegin; i < keysIteratorEnd; ++i) {;
//        std::cout << allIndices[*i] << '\n';
//    }


    //thrust::copy(data.begin(), data.end(), std::ostream_iterator<int>(std::cout, " ")); std::cout << std::endl;
    //[-------------------array of data-----------------]
    //[-------------------array of indices--------------]
    //[-------------------array of offsets--------------]
    // for each offset
    //     sum = 0
    //     for each index
    //        sum += data[offset+index]
    //permutation_iterator, zip_iterator, transform_iterator, make_tuple<ot, pos, station>

    //thrust::copy(sumAllValuesPerPosition.begin(), sumAllValuesPerPosition.end(), result.begin());
}
