#include "Crustal.h"

std::unique_ptr<Crustal> Crustal::instance_;

void Crustal::addLayer(Layer layer)
{
    layers_.push_back(layer);
    layersCount_ = layers_.size();
    sortLayers();
    computeThickness();
}

Crustal::Crustal()
{}

void Crustal::addLayer(float depth, float pWaveSpeed, float sWaveSpeed)
{
    layers_.push_back(Layer{depth, pWaveSpeed, sWaveSpeed});
    layersCount_ = layers_.size();
    sortLayers();
    computeThickness();
}

void Crustal::computeThickness()
{
    thickness_.clear();
    for (auto i =  size_t{0}; i < layersCount_; ++i) {
        if (i == 0) {
            thickness_.push_back(layers_[i].depth);
        } else {
           thickness_.push_back(layers_[i].depth - layers_[i-1].depth);
        }
    }
}


std::vector<Layer>& Crustal::getLayers()
{
    return layers_;
}


void Crustal::sortLayers()
{
    if (layersCount_ <= 1) {
        return;
    }
    std::sort(layers_.begin(), layers_.end());
}

Crustal& Crustal::getInstance()
{
    if (instance_.get() == nullptr) {
         instance_ = std::unique_ptr<Crustal>(new Crustal{});
    }
    return *instance_;
}

Layer Crustal::getLayer(float depth)  const
{
    for (auto i = size_t{1}; i < layersCount_; ++i) {
        auto& layer = layers_[i];
        if (depth < layer.depth) {
            //currentLayer depth and previous layer's speeds as in MODIF.FOR
            return Layer{layer.depth, layers_[i-1].speed.pWave, layers_[i-1].speed.sWave};
        }
    }

    //huge layer depth and last layer's speeds as in MODIF.FOR
    return Layer{90, layers_[layersCount_-1].speed.pWave, layers_[layersCount_-1].speed.sWave};
}

Layer Crustal::getLayer(float depth, size_t& index) const
{
    for (auto i = size_t{1}; i < layersCount_; ++i) {
        auto& layer = layers_[i];
        if (depth < layer.depth) {
            index = i;
            //currentLayer depth and previous layer's speeds as in MODIF.FOR
            return Layer{layer.depth, layers_[i-1].speed.pWave, layers_[i-1].speed.sWave};
        }
    }
    if (depth >= 90) {
        index = -1;
        return Layer{};
    }
    index = layersCount_ - 1;
    //huge layer depth and last layer's speeds as in MODIF.FOR
    return Layer{90.0f, layers_[layersCount_-1].speed.pWave, layers_[layersCount_-1].speed.sWave};
}

bool Layer::operator!=(const Layer& right) const
{
    return !(*this == right);
}

bool Layer::operator<(const Layer& right) const
{
    return depth < right.depth;
}

bool Layer::operator<=(const Layer& right) const
{
    return depth <= right.depth;
}

bool Layer::operator==(const Layer& right) const
{
    return depth == right.depth;
}

bool Layer::operator>(const Layer& right) const
{
    return depth > right.depth;
}

bool Layer::operator>=(const Layer& right) const
{
    return depth >= right.depth;
}

float Crustal::getThickness(size_t layerIndex) const
{
    return thickness_[layerIndex];
}