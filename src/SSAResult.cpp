#include "SSAResult.h"

SSAResult::Info::Info(
    float x_, float y_, float z_, size_t stationsUsedP_, size_t stationsUsedS_,
    float sumP_, float sumS_, float goodP_, float goodS_, float goodPs_
) :
    x(x_), y(y_), z(z_), stationsUsedP(stationsUsedP_),
    stationsUsedS(stationsUsedS_), sumP(sumP_), sumS(sumS_),
    goodP(goodP_), goodS(goodS_), goodPs(goodPs_)
{}

SSAResult::Info::Info() :
    x(std::numeric_limits<float>::infinity()),
    y(std::numeric_limits<float>::infinity()),
    z(std::numeric_limits<float>::infinity()),
    stationsUsedP(0), stationsUsedS(0), sumP(0), sumS(0),
    goodP(0), goodS(0), goodPs(0)
{}

SSAResult::SSAResult() :
    infos_(),
    maximumPWaveBrightnessInfoIndex_(-1),
    maximumSWaveBrightnessInfoIndex_(-1),
    maximumPsWaveBrightnessInfoIndex_(-1),
    ratNumP_(0),
    ratGoodP_(0),
    ratNumS_(0),
    ratGoodS_(0),
    ratNumPs_(0),
    ratGoodPs_(0),
    numP_(0),
    numS_(0),
    numPS_(0)
{}


void SSAResult::addInfo(
    float x, float y, float z, size_t stationsUsedP, size_t stationsUsedS,
    float sumP, float sumS, float goodP, float goodS, float goodPs
)
{
    infos_.push_back(
        make_unique<Info>(
            x, y, z, stationsUsedP, stationsUsedS, sumP, sumS, goodP, goodS, goodPs
        )
    );
}

const SSAResult::Info& SSAResult::getInfoAt(size_t index) const
{
    return *infos_[index];
}

const SSAResult::Info& SSAResult::getMaximumPWaveBrightnessInfo() const
{
    return getInfoAt(maximumPWaveBrightnessInfoIndex_);
}

const SSAResult::Info& SSAResult::getMaximumPsWaveBrightnessInfo() const
{
    return getInfoAt(maximumPsWaveBrightnessInfoIndex_);
}

const SSAResult::Info& SSAResult::getMaximumSWaveBrightnessInfo() const
{
    return getInfoAt(maximumSWaveBrightnessInfoIndex_);
}

const size_t SSAResult::getMaximumPWaveBrightnessInfoIndex() const
{
    return maximumPWaveBrightnessInfoIndex_;
}

const size_t SSAResult::getMaximumPsWaveBrightnessInfoIndex() const
{
    return maximumPsWaveBrightnessInfoIndex_;
}

const size_t SSAResult::getMaximumSWaveBrightnessInfoIndex() const
{
    return maximumSWaveBrightnessInfoIndex_;
}

const std::vector<std::unique_ptr<SSAResult::Info> >& SSAResult::getInfos() const
{
    return infos_;
}

float SSAResult::getRatNumP()  const
{
    return ratNumP_;
}

float SSAResult::getRatNumS()  const
{
    return ratNumS_;
}

float SSAResult::getRatNumPs()  const
{
    return ratNumPs_;
}

float SSAResult::getRatGoodP()  const
{
    return ratGoodP_;
}

float SSAResult::getRatGoodS()  const
{
    return ratGoodS_;
}

float SSAResult::getRatGoodPs()  const
{
    return ratGoodPs_;
}

float SSAResult::getExecutionTime() const
{
    return executionTime_;
}

const size_t SSAResult::getNumP() const
{
    return numP_;
}

const size_t SSAResult::getNumS() const
{
    return numS_;
}

const size_t SSAResult::getNumPS() const
{
    return numPS_;
}


