#include "ParallelSSA.h"

#include <chrono>
#include <iostream>

#define VERBOSE 1
#include <thrust/host_vector.h>

#include "Crustal.h"
#include "SSA.h"
#include <fstream>

#include "compute_ots.h"


ParallelSSA::ParallelSSA(SSA& t_ssa, float otIncrement, float samplingInterval, std::string cacheFile)
    : ssa_{t_ssa}
    , values_{}
    , offsets_{}
    , travelTimesComputed_{false}
    , otIncrement_{otIncrement}
    , samplingInterval_{samplingInterval}
    , cacheFile_{cacheFile}
{
}

void ParallelSSA::check_cache()
{
    if (travelTimesComputed_) {
        return;
    }
    std::ifstream cache_file{cacheFile_, std::ios::binary};
    if (!cache_file.good()) {
        return;
    }
    read_from_cache(cache_file);
}

void ParallelSSA::read_from_cache(std::ifstream& file)
{

    if (travelTimesComputed_) {
        return;
    }
    auto start = std::chrono::system_clock::now();
    std::cout << "Reading from cache...\n";
    size_t size = 0;
    file.read(reinterpret_cast<char*>(&size), sizeof_cache_header);

    values_.clear();
    values_.resize(size);
    file.read(reinterpret_cast<char*>(&values_[0]), size * sizeof(float));

    size_t offsetSize = 0;
    file.read(reinterpret_cast<char*>(&offsetSize), sizeof_cache_header);
    offsets_.clear();
    offsets_.resize(offsetSize);
    file.read(reinterpret_cast<char*>(&offsets_[0]), size * sizeof(size_t));

    travelTimesComputed_ = true;

    auto end        = std::chrono::system_clock::now();
    auto timeToRead = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() / 1000.0f;
    std::cout << "Lido do cache em " << timeToRead << "s\n";
}

void ParallelSSA::write_to_cache()
{
    if (!travelTimesComputed_) {
        return;
    }
    std::ofstream cache_file{cacheFile_, std::ios::binary};
    auto size       = values_.size();
    auto offsetSize = offsets_.size();

    cache_file.write(reinterpret_cast<const char*>(&size), sizeof_cache_header);
    cache_file.write(reinterpret_cast<const char*>(&values_[0]), sizeof(float) * size);

    cache_file.write(reinterpret_cast<const char*>(&offsetSize), sizeof_cache_header);
    cache_file.write(reinterpret_cast<const char*>(&offsets_[0]), sizeof(size_t) * size);
}


void ParallelSSA::compute_travel_times(Crustal& crustal)
{
    //check_cache();
    if (travelTimesComputed_) {
        return;
    }
    auto& layers    = crustal.getLayers();
    auto layerCount = crustal.layersCount();

    size_t maxOffset = 0;

    auto startFillArrays = std::chrono::system_clock::now();


    for (size_t iZ = 0; iZ < ssa_.getZExtent().getNumberOfSamples(); iZ++) {
        auto z = ssa_.getZExtent().min + (iZ * ssa_.getZExtent().step);
        auto currentLayerIndex = size_t{};
        auto currentLayer      = crustal.getLayer(z, currentLayerIndex);


        for (size_t iX = 0; iX < ssa_.getXExtent().getNumberOfSamples(); iX++) {
            auto x = ssa_.getXExtent().min + (iX * ssa_.getXExtent().step);
            for (size_t iY = 0; iY < ssa_.getYExtent().getNumberOfSamples(); iY++) {
                auto y = ssa_.getYExtent().min + (iY * ssa_.getYExtent().step);

                for (auto& stationPtr : ssa_.getStations()) {

                    auto& station = *stationPtr;
                    auto travelTime = station.computeTravelTimeToPoint(
                        currentLayer,
                        currentLayerIndex,
                        layers,
                        layerCount,
                        crustal,
                        x, y, z);

                    auto pIndex = static_cast<size_t>(travelTime.pWave / samplingInterval_);

                    offsets_.push_back(pIndex);
                    if (maxOffset < pIndex) {
                        maxOffset = pIndex;
                    }
                }
            }
        }
    }
#ifdef VERBOSE
    std::cout << "Min acceptable securityMargin: " << maxOffset << "\n";
#endif

    travelTimesComputed_ = true;
    //write_to_cache();

    auto endFillArrays    = std::chrono::system_clock::now();
    auto timeToFillArrays = std::chrono::duration_cast<std::chrono::milliseconds>(endFillArrays - startFillArrays).count() / 1000.0f;
#ifdef VERBOSE
    std::cout << "Time to compute travel times: " << timeToFillArrays << "\n";
#endif
}


std::unique_ptr<thrust::host_vector<float>> ParallelSSA::compute(
        Crustal& crustal, size_t otAmount, size_t firstOt,
        bool isLast, bool total, size_t securityMargin
) {
    //values_.clear();

    //fill array with values of each station of each position of each OT
    compute_travel_times(crustal);

    values_.clear();


#ifdef VERBOSE
    std::cout << "Sending " << (otAmount + securityMargin) << " logs of " << ssa_.getStations().size() << " station(s) (first ot: " << firstOt << ")" << "\n";
#endif
    for (auto& station : ssa_.getStations()) {
        for (size_t i = firstOt; i < (firstOt + otAmount + securityMargin); ++i) {
            values_.push_back(station->getLogs()[i].pWave);
        }
        /* values_.reserve(station->getLogs().size()); */
        /* for (auto& log : station->getLogs()) { */
        /*     values_.push_back(log.pWave); */
        /* } */
    }


    auto result = make_unique<thrust::host_vector<float>>(otAmount * ssa_.getNumberOfSamples());


    auto startCompute = std::chrono::system_clock::now();

    /* compute_ots( */
    /*     static_cast<size_t>(ssa_.getNumberOfStations()), */
    /*     ssa_.getStationAt(0).getLogs().size(), */
    /*     static_cast<size_t>(ssa_.getNumberOfSamples()), */
    /*     otAmount, */
    /*     /1* firstOt, *1/ */
    /*     firstOt, */
    /*     values_, */
    /*     offsets_, */
    /*     *result); */
    compute_ots(
        static_cast<size_t>(ssa_.getNumberOfStations()),
        otAmount + securityMargin,
        static_cast<size_t>(ssa_.getNumberOfSamples()),
        otAmount,
        0,
        values_,
        offsets_,
        *result);

    auto endCompute = std::chrono::system_clock::now();

    auto timeToCompute = std::chrono::duration_cast<std::chrono::milliseconds>(endCompute - startCompute).count() / 1000.0f;


    //std::cout << "Time to process OTs(" << otAmount << "): " << timeToCompute << ", Size:" << (*result).size() << ", First value:" << (*result)[0] << ", Last value: " << (*result)[(*result).size() -1] << "\n";
#ifdef VERBOSE
    std::cout << "Time to sum OTs(" << otAmount << "): " << timeToCompute << ", Size:" << (*result).size() << "\n";
#endif
    return result;
}
